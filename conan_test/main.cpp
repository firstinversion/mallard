#include <iostream>
#include <mallard/migration.h>

int main() {
    mallard::migration m;
    m.name("testpack");

    std::cout << "Package: " << m.name() << '\n';

    return 0;
}
