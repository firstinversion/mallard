#pragma once

#include <string>
#include <vector>

namespace mallard {

class migration {
public:
    migration() = default;
    explicit migration(std::string name);
    explicit migration(const char* name);

    const std::string& name() const;
    const std::string& description() const;
    const std::vector<std::string>& requires_() const;
    const std::string& script_text() const;
    const std::string& checksum() const;

    migration& name(std::string name);
    migration& description(std::string description);
    migration& requires_(std::vector<std::string> requires_);
    migration& script_text(std::string scriptText);

    migration& set_name(std::string name);
    migration& set_description(std::string description);
    migration& set_requires_(std::vector<std::string> requires_);
    migration& set_script_text(std::string script_text);

    friend bool operator==(const migration& lhs, const migration& rhs);
    friend bool operator!=(const migration& lhs, const migration& rhs);
    friend bool operator<(const migration& lhs, const migration& rhs);
    friend bool operator>(const migration& lhs, const migration& rhs);
    friend bool operator<=(const migration& lhs, const migration& rhs);
    friend bool operator>=(const migration& lhs, const migration& rhs);

private:
    std::string _name;
    std::string _description;
    std::vector<std::string> _requires;
    std::string _script_text;

    std::string _checksum; // Set when script_text is set.
};

}
