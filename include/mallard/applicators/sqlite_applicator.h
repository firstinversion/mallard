#pragma once

#include <SQLiteCpp/SQLiteCpp.h>
#include <mallard/migration.h>
#include <mallard/migration_plan.h>
#include <mallard/applied_migration.h>

namespace mallard {

class sqlite_applicator {
public:
    explicit sqlite_applicator(const std::string& connection_string, bool auto_initialize = true);
    explicit sqlite_applicator(std::unique_ptr<SQLite::Database> session, bool auto_initialize = true);
    void initialize();

    int64_t find_version();
    template<typename OutputItr>
    void find_applied_migrations(OutputItr out);
    void apply(const migration_plan& plan);

    SQLite::Database& session();
    std::unique_ptr<SQLite::Database> extract_session();

private:
    void setup_version_1();

    void record_migration(const migration& migration);
    std::string stringify_requires(const std::vector<std::string>& req);

    std::unique_ptr<SQLite::Database> _session;
};

template<typename OutputItr>
void sqlite_applicator::find_applied_migrations(OutputItr out) {
    if (!_session) throw applicator_missing_session_error();
    SQLite::Statement query(*_session, "SELECT id, name, checksum FROM mallard_applied_migration;");

    while (query.executeStep()) {
        out = applied_migration()
                .database_id(query.getColumn(0).getInt())
                .migration_name(query.getColumn(1).getString())
                .checksum(query.getColumn(2).getString());
    }
}

}
