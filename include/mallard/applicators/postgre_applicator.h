#pragma once

#include <functional>
#include <pqxx/pqxx>
#include <mallard/migration.h>
#include <mallard/migration_plan.h>
#include <mallard/applied_migration.h>

namespace mallard {

class postgre_applicator {
public:
    explicit postgre_applicator(pqxx::connection& session, bool auto_initialize = true);
    void initialize();

    int32_t find_version();
    template<typename OutputItr>
    void find_applied_migrations(OutputItr out);

    static void default_on_apply(const migration&) {}
    void apply(const migration_plan& plan, std::function<void(const migration&)> on_apply = default_on_apply);

private:
    void setup_version_1();

    std::string stringify_requires(const std::vector<std::string>& req);
    void record_migration(pqxx::work& transaction, const migration& migration);

    pqxx::connection& _session;
};

template<typename OutputItr>
void postgre_applicator::find_applied_migrations(OutputItr out) {
    pqxx::work transaction(_session);
    pqxx::result rs = transaction.exec("SELECT id, name, checksum FROM mallard.applied_migration;");
    transaction.commit();

    std::transform(rs.begin(), rs.end(), out, [](pqxx::row row) {
        return applied_migration()
                .database_id(row[0].as<int32_t>())
                .migration_name(row[1].as<std::string>())
                .checksum(row[2].as<std::string>());
    });
}

}
