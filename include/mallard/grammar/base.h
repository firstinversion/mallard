#pragma once

#include <boost/spirit/include/qi.hpp>
#include <boost/phoenix.hpp>
#include <boost/variant.hpp>

namespace mallard {

namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;
namespace phx = boost::phoenix;
using namespace qi::labels;

template<typename Iterator>
struct mallard_default_skipper : qi::grammar<Iterator> {
    mallard_default_skipper()
            : mallard_default_skipper::base_type(skip) {
        skip = ascii::space | '-';
    }

    qi::rule<Iterator> skip;
};

template<typename Iterator>
struct mallard_temp_test_skipper : qi::grammar<Iterator> {
    mallard_temp_test_skipper()
            : mallard_temp_test_skipper::base_type(skip) {
        skip = ascii::space | ascii::blank | "--" | '\n';
    }

    qi::rule<Iterator> skip;
};

}
