#pragma once

#include <mallard/grammar/base.h>
#include <mallard/migration.h>

namespace mallard {

template<typename Iterator, typename Skipper = mallard_temp_test_skipper<Iterator>>
struct migration_grammar : qi::grammar<Iterator, migration(), Skipper> {
    migration_grammar()
            : migration_grammar::base_type(migration_rule, "migration_rule") {

        word_rule %= qi::lexeme[+(qi::char_ - ascii::blank - '\n' - ',' - ';' - ']')];
        line_rule %= qi::lexeme[+(qi::char_ - '\n' - ',' - ';' - ']')];
        until_next_rule %= qi::no_skip[+(qi::char_ - '#')];
        prefix_rule %= qi::no_skip[*(qi::char_ - '#')];
        list_content_rule %= word_rule % ',';
        list_rule %= '[' > list_content_rule > ']';
        empty_list_rule %= qi::lit('[') > qi::lit(']');

        name_rule %= "name:" > word_rule || ',';
        description_rule %= "description:" > line_rule || ',';
        requires_rule %= "requires:" > (list_rule | empty_list_rule) || ',';
        header_rule = *(name_rule[phx::bind(&migration::set_name, _r1, _1)] |
                        description_rule[phx::bind(&migration::set_description, _r1, _1)] |
                        requires_rule[phx::bind(&migration::set_requires_, _r1, _1)]);
        body_rule = until_next_rule[phx::bind(&migration::set_script_text, _r1, _1)];
        migration_rule = -prefix_rule > "#!migration" > header_rule(_val) > ';' || body_rule(_val);
        migration_list_rule %= *migration_rule;

        word_rule.name("word_rule");
        line_rule.name("line_rule");
        until_next_rule.name("until_next_rule");
        prefix_rule.name("prefix_rule");
        list_content_rule.name("list_content_rule");
        list_rule.name("list_rule");
        empty_list_rule.name("empty_list_rule");

        name_rule.name("name_rule");
        description_rule.name("description_rule");
        requires_rule.name("requires_rule");
        header_rule.name("header_rule");
        body_rule.name("body_rule");
        migration_rule.name("migration_rule");
        migration_list_rule.name("migration_list_rule");

        qi::on_error<qi::fail>
                (migration_rule,
                 error << phx::val("Error! Expecting") << _4 << phx::val("here: \"")
                       << phx::construct<std::string>(_3, _2)
                       << phx::val("\""));

        qi::on_error<qi::fail>
                (migration_list_rule,
                 error << phx::val("Error! Expecting") << _4 << phx::val("here: \"")
                       << phx::construct<std::string>(_3, _2)
                       << phx::val("\""));
    }

    qi::rule<Iterator, std::string(), Skipper> word_rule;
    qi::rule<Iterator, std::string(), Skipper> line_rule;
    qi::rule<Iterator, std::string(), Skipper> until_next_rule;
    qi::rule<Iterator, void(), Skipper> prefix_rule;
    qi::rule<Iterator, std::vector<std::string>(), Skipper> list_content_rule;
    qi::rule<Iterator, std::vector<std::string>(), Skipper> list_rule;
    qi::rule<Iterator, std::vector<std::string>(), Skipper> empty_list_rule;

    qi::rule<Iterator, std::string(), Skipper> name_rule;
    qi::rule<Iterator, std::string(), Skipper> description_rule;
    qi::rule<Iterator, std::vector<std::string>(), Skipper> requires_rule;
    qi::rule<Iterator, void(migration&), Skipper> header_rule;
    qi::rule<Iterator, void(migration&), Skipper> body_rule;
    qi::rule<Iterator, migration(), Skipper> migration_rule;
    qi::rule<Iterator, std::vector<migration>(), Skipper> migration_list_rule;

    std::stringstream error;
};

}
