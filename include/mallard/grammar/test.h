#pragma once

#include <mallard/grammar/base.h>
#include <mallard/test.h>
#include <sstream>

namespace mallard {

template<typename Iterator, typename Skipper = mallard_temp_test_skipper<Iterator>>
struct test_grammar : qi::grammar<Iterator, test(), Skipper> {
    test_grammar() : test_grammar::base_type(test_rule, "test_rule") {

        word_rule %= qi::lexeme[+(qi::char_ - ascii::space - qi::eol - ',' - ';')];
        line_rule %= qi::lexeme[+(qi::char_ - qi::eol - ',' - ';')];
        until_next_rule %= qi::lexeme[+(qi::char_ - "#!")];

        name_rule %= "name:" > word_rule || ',';
        description_rule %= "description:" > line_rule || ',';
        header_rule = *(name_rule[phx::bind(&test::set_name, _r1, _1)] |
                        description_rule[phx::bind(&test::set_description, _r1, _1)]);
        body_rule = until_next_rule[phx::bind(&test::set_script_text, _r1, _1)];
        test_rule = "#!test" > header_rule(_val) > ';' || body_rule(_val);
        test_list_rule %= *test_rule;

        word_rule.name("word_rule");
        line_rule.name("line_rule");
        until_next_rule.name("until_next_rule");

        name_rule.name("name_rule");
        description_rule.name("description_rule");
        header_rule.name("header_rule");
        body_rule.name("body_rule");
        test_rule.name("test_rule");
        test_list_rule.name("test_name_rule");

        qi::on_error<qi::fail>
                (test_rule,
                 error << phx::val("Error! Expecting") << _4 << phx::val("here: \"")
                       << phx::construct<std::string>(_3, _2)
                       << phx::val("\""));

        qi::on_error<qi::fail>
                (test_list_rule,
                 error << phx::val("Error! Expecting") << _4 << phx::val("here: \"")
                       << phx::construct<std::string>(_3, _2)
                       << phx::val("\""));
    }

    qi::rule<Iterator, std::string(), Skipper> word_rule;
    qi::rule<Iterator, std::string(), Skipper> line_rule;
    qi::rule<Iterator, std::string(), Skipper> until_next_rule;

    qi::rule<Iterator, std::string(), Skipper> name_rule;
    qi::rule<Iterator, std::string(), Skipper> description_rule;
    qi::rule<Iterator, void(test&), Skipper> header_rule;
    qi::rule<Iterator, void(test&), Skipper> body_rule;
    qi::rule<Iterator, test(), Skipper> test_rule;
    qi::rule<Iterator, std::vector<test>(), Skipper> test_list_rule;
    std::stringstream error;
};

}
