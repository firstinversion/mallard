#pragma once

#include <sstream>
#include <map>
#include <boost/filesystem.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/phoenix.hpp>
#include <boost/variant.hpp>
#include <boost/iostreams/device/mapped_file.hpp>

#include <mallard/migration.h>
#include <mallard/migration_plan.h>
#include <mallard/error.h>
#include <mallard/grammar/migration.h>

namespace mallard {

template<typename InputItr, typename OutputItr>
void parse_migration_from(InputItr first, InputItr last, OutputItr output) {
    migration_grammar<InputItr> grammar;
    mallard_default_skipper<InputItr> skipper;

    while (first != last) {    // itr is incremented by phrase_parse.
        migration last_migration;
        if (boost::spirit::qi::phrase_parse(first, last, grammar.migration_rule, skipper, last_migration)) {
            output = std::move(last_migration);
        } else {
            throw parsing_error(grammar.error.str());
        }
    }
}

template<typename OutputItr>
void parse_migration_from_string(const std::string& buffer, OutputItr output) {
    parse_migration_from(buffer.cbegin(), buffer.cend(), output);
}

template<typename OutputItr>
void parse_migration_from_file(const std::string& path, OutputItr output) {
    if (!boost::filesystem::exists(path)) throw std::invalid_argument("File not found: " + path);

    boost::iostreams::mapped_file file(path);
    if (!file.is_open()) throw std::runtime_error("Unable to open file: " + path);

    parse_migration_from(file.const_begin(), file.const_end(), output);
}

template<typename OutContainer = migration_plan>
OutContainer parse_migration_from_file(const std::string& path) {
    OutContainer output;
    parse_migration_from_file(path, std::inserter(output, output.end()));
    return output;
}

template<typename OutContainer = migration_plan>
OutContainer parse_migration_from_string(const std::string& buffer) {
    OutContainer output;
    parse_migration_from_string(buffer, std::inserter(output, output.end()));
    return output;
}

#ifdef MALLARD_PRECOMPILE_GRAMMAR

// String Iterators

extern template
class mallard_default_skipper<std::string::iterator>;

extern template
class mallard_default_skipper<std::string::const_iterator>;

extern template
class mallard_default_skipper<std::string::reverse_iterator>;

extern template
class mallard_default_skipper<std::string::const_reverse_iterator>;

extern template
class migration_grammar<std::string::iterator, mallard_default_skipper<std::string::iterator>>;

extern template
class migration_grammar<std::string::const_iterator, mallard_default_skipper<std::string::const_iterator>>;

extern template
class migration_grammar<std::string::reverse_iterator, mallard_default_skipper<std::string::reverse_iterator>>;

extern template
class migration_grammar<std::string::const_reverse_iterator, mallard_default_skipper<std::string::const_reverse_iterator>>;

// Mapped File Iterators

extern template
class mallard_default_skipper<boost::iostreams::mapped_file::iterator>;

extern template
class mallard_default_skipper<boost::iostreams::mapped_file::const_iterator>;

extern template
class migration_grammar<boost::iostreams::mapped_file::iterator,
        mallard_default_skipper<boost::iostreams::mapped_file::iterator>>;

extern template
class migration_grammar<boost::iostreams::mapped_file::const_iterator,
        mallard_default_skipper<boost::iostreams::mapped_file::const_iterator>>;

#endif

}
