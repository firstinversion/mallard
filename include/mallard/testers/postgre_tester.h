#pragma once

#include <pqxx/pqxx>
#include <mallard/test.h>
#include <mallard/test_plan.h>
#include <mallard/error.h>

namespace mallard {

class postgre_tester {
public:
    explicit postgre_tester(pqxx::connection& session);

    void apply(const test_plan& plan);

private:
    pqxx::connection& _session;
};

}
