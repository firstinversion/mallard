#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <mallard/migration.h>
#include <mallard/error.h>

namespace mallard {

class migration_plan {
public:
    using graph_type = boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS>;
    using edge_type = std::pair<size_t, size_t>;

    using iterator = std::vector<migration>::iterator;
    using const_iterator = std::vector<migration>::const_iterator;
    using reverse_iterator = std::vector<migration>::reverse_iterator;
    using const_reverse_iterator = std::vector<migration>::const_reverse_iterator;
    using size_type = std::vector<migration>::size_type;
    using value_type = migration;

    class topological_iterator : public std::iterator<std::input_iterator_tag,
            migration,
            ptrdiff_t,
            const migration*,
            const migration&> {
    public:
        topological_iterator(const std::vector<migration>& migrations,
                             std::vector<size_t>::const_iterator base_iterator);

        const migration& operator*() const;
        const migration* operator->() const;
        topological_iterator& operator++();
        const topological_iterator operator++(int);

        friend bool operator==(const topological_iterator& lhs, const topological_iterator& rhs);
        friend bool operator!=(const topological_iterator& lhs, const topological_iterator& rhs);

    private:
        const std::vector<migration>& _migrations;
        std::vector<size_t>::const_iterator _base_iterator;
    };

    // Constructors

    migration_plan();
    explicit migration_plan(std::vector<migration> migrations);
    migration_plan(std::initializer_list<migration> migrations);

    // Primary Data Manipulation

    migration& at(size_t idx);
    const migration& at(size_t idx) const;
    migration& operator[](size_t idx);
    const migration& operator[](size_t idx) const;
    migration& front();
    const migration& front() const;
    migration& back();
    const migration& back() const;
    migration* data() noexcept;
    const migration* data() const noexcept;

    iterator begin() noexcept;
    const_iterator begin() const noexcept;
    const_iterator cbegin() const noexcept;
    iterator end() noexcept;
    const_iterator end() const noexcept;
    const_iterator cend() const noexcept;
    reverse_iterator rbegin() noexcept;
    const_reverse_iterator rbegin() const noexcept;
    const_reverse_iterator crbegin() const noexcept;
    reverse_iterator rend() noexcept;
    const_reverse_iterator rend() const noexcept;
    const_reverse_iterator crend() const noexcept;

    bool empty() const noexcept;
    size_type size() const noexcept;
    size_type max_size() const noexcept;
    void reserve(size_type new_cap);
    size_type capacity() const noexcept;
    void shrink_to_fit();

    void clear() noexcept;
    iterator insert(const_iterator pos, migration value);
    iterator insert(const_iterator pos, size_type count, const migration& value);
    template<typename InputIt>
    iterator insert(const_iterator pos, InputIt first, InputIt last);
    iterator insert(const_iterator pos, std::initializer_list<migration> ilist);
    template<typename... Args>
    iterator emplace(const_iterator pos, Args&& ... args);
    iterator erase(const_iterator pos);
    iterator erase(const_iterator first, const_iterator last);
    void push_back(migration value);
    template<typename... Args>
    migration& emplace_back(Args&& ... args);
    void pop_back();
    void resize(size_type count);
    void resize(size_type count, const migration& value);
    void swap(migration_plan& other) noexcept;

    // Comparison Operators

    friend bool operator==(const migration_plan& lhs, const migration_plan& rhs);
    friend bool operator!=(const migration_plan& lhs, const migration_plan& rhs);
    friend bool operator<(const migration_plan& lhs, const migration_plan& rhs);
    friend bool operator>(const migration_plan& lhs, const migration_plan& rhs);
    friend bool operator<=(const migration_plan& lhs, const migration_plan& rhs);
    friend bool operator>=(const migration_plan& lhs, const migration_plan& rhs);

    // Validation

    bool valid() const;
    errors validate();
    bool validate_no_name_overlap(errors& report);
    bool validate_no_missing_dependency(errors& report);
    bool validate_no_dependency_cycle(errors& report);

    // Topological Traversal
    topological_iterator tbegin() const;
    topological_iterator tend() const;

private:
    // Primary Data
    std::vector<migration> _migrations;

    // State Data
    bool _is_valid;

    // Calculated Data
    std::unordered_map<std::string, size_t> _names;
    graph_type _graph;
    std::vector<size_t> _topological_ordering;
};

template<typename InputIt>
migration_plan::iterator migration_plan::insert(migration_plan::const_iterator pos, InputIt first, InputIt last) {
    _is_valid = false;
    return _migrations.insert(pos, first, last);
}

template<typename... Args>
migration_plan::iterator migration_plan::emplace(migration_plan::const_iterator pos, Args&& ... args) {
    _is_valid = false;
    return _migrations.emplace<Args...>(pos, std::forward<Args>(args)...);
}

template<typename... Args>
migration& migration_plan::emplace_back(Args&& ... args) {
    _is_valid = false;
    return _migrations.emplace_back<Args...>(std::forward<Args>(args)...);
}

}
