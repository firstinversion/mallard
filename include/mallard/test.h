#pragma once

#include <string>
#include <string_view>

namespace mallard {

class test {
public:
    test() = default;
    explicit test(std::string_view name);
    explicit test(const char* name);

    const std::string& name() const;
    const std::string& description() const;
    const std::string& script_text() const;

    test& name(std::string_view name);
    test& description(std::string_view description);
    test& script_text(std::string_view script_text);

    test& set_name(std::string_view name);
    test& set_description(std::string_view description);
    test& set_script_text(std::string_view script_text);

    friend bool operator==(const test& lhs, const test& rhs);
    friend bool operator!=(const test& lhs, const test& rhs);
    friend bool operator<(const test& lhs, const test& rhs);
    friend bool operator>(const test& lhs, const test& rhs);
    friend bool operator<=(const test& lhs, const test& rhs);
    friend bool operator>=(const test& lhs, const test& rhs);

private:
    std::string _name;
    std::string _description;
    std::string _script_text;
};

}
