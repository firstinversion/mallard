#pragma once

#include <string>
#include <vector>
#include <exception>
#include <memory>

namespace mallard {

class error : public std::exception {
};

using errors = std::vector<std::unique_ptr<error>>;

class name_overlap_error : public error {
public:
    explicit name_overlap_error(std::string name);

    const char* what() const noexcept override;
    const std::string& name();

private:
    std::string _name;
    std::string _what;
};

class missing_dependency_error : public error {
public:
    missing_dependency_error(std::string migration_name, std::string dependency_name);

    const char* what() const noexcept override;
    const std::string& migration_name();
    const std::string& dependency_name();

private:
    std::string _migration_name;
    std::string _dependency_name;
    std::string _what;
};

class dependency_cycle_error : public error {
public:
    explicit dependency_cycle_error(std::vector<std::string> names);

    const char* what() const noexcept override;
    const std::vector<std::string>& getNames() const;

private:
    std::vector<std::string> _names;
    std::string _what;
};

class parsing_error : public error {
public:
    parsing_error() = default;
    explicit parsing_error(std::string what);
    explicit parsing_error(const char* what);
    const char* what() const noexcept override;

private:
    std::string _what;
};

class applicator_missing_session_error : public error {
public:
    applicator_missing_session_error() = default;
    const char* what() const noexcept override;
};

class applicator_sql_error : public error {
public:
    applicator_sql_error() = default;
    applicator_sql_error(std::string migration_name, const std::exception& sqlerror);
    const char* what() const noexcept override;

private:
    std::string _migration_name;
    std::string _what;
};

class tester_sql_error : public error {
public:
    tester_sql_error() = default;
    tester_sql_error(std::string test_name, const std::exception& sqlerror);
    const char* what() const noexcept override;

private:
    std::string _test_name;
    std::string _what;
};

}
