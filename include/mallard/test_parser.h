#pragma once

#include <sstream>
#include <map>
#include <boost/filesystem.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/phoenix.hpp>
#include <boost/variant.hpp>
#include <boost/iostreams/device/mapped_file.hpp>

#include <mallard/test.h>
#include <mallard/test_plan.h>
#include <mallard/error.h>
#include <mallard/grammar/test.h>

namespace mallard {

template<typename InputItr, typename OutputItr>
void parse_test_from(InputItr first, InputItr last, OutputItr output) {
    test_grammar<InputItr> grammar;
    mallard_temp_test_skipper<InputItr> skipper;

    while (first != last) {    // itr is incremented by phrase_parse.
        test last_test;
        if (boost::spirit::qi::phrase_parse(first, last, grammar.test_rule, skipper, last_test)) {
            output = std::move(last_test);
        } else {
            throw parsing_error(grammar.error.str());
        }
    }
}

template<typename OutputItr>
void parse_test_from_string(const std::string& buffer, OutputItr output) {
    parse_test_from(buffer.cbegin(), buffer.cend(), output);
}

template<typename OutputItr>
void parse_test_from_file(const std::string& path, OutputItr output) {
    if (!boost::filesystem::exists(path)) throw std::invalid_argument("File not found: " + path);

    boost::iostreams::mapped_file file(path);
    if (!file.is_open()) throw std::runtime_error("Unable to open file: " + path);

    parse_test_from(file.const_begin(), file.const_end(), output);
}

template<typename OutContainer = test_plan>
OutContainer parse_test_from_file(const std::string& path) {
    OutContainer output;
    parse_test_from_file(path, std::inserter(output, output.end()));
    return output;
}

template<typename OutContainer = test_plan>
OutContainer parse_test_from_string(const std::string& buffer) {
    OutContainer output;
    parse_test_from_string(buffer, std::inserter(output, output.end()));
    return output;
}

#ifdef MALLARD_PRECOMPILE_GRAMMAR

// String Iterators

extern template
class mallard_temp_test_skipper<std::string::iterator>;

extern template
class mallard_temp_test_skipper<std::string::const_iterator>;

extern template
class mallard_temp_test_skipper<std::string::reverse_iterator>;

extern template
class mallard_temp_test_skipper<std::string::const_reverse_iterator>;

extern template
class test_grammar<std::string::iterator, mallard_temp_test_skipper<std::string::iterator>>;

extern template
class test_grammar<std::string::const_iterator, mallard_temp_test_skipper<std::string::const_iterator>>;

extern template
class test_grammar<std::string::reverse_iterator, mallard_temp_test_skipper<std::string::reverse_iterator>>;

extern template
class test_grammar<std::string::const_reverse_iterator, mallard_temp_test_skipper<std::string::const_reverse_iterator>>;

// Mapped File Iterators

extern template
class mallard_temp_test_skipper<boost::iostreams::mapped_file::iterator>;

extern template
class mallard_temp_test_skipper<boost::iostreams::mapped_file::const_iterator>;

extern template
class test_grammar<boost::iostreams::mapped_file::iterator,
        mallard_temp_test_skipper<boost::iostreams::mapped_file::iterator>>;

extern template
class test_grammar<boost::iostreams::mapped_file::const_iterator,
        mallard_temp_test_skipper<boost::iostreams::mapped_file::const_iterator>>;

#endif

}
