#pragma once

#include <chrono>
#include <mallard/migration.h>

namespace mallard {

class applied_migration {
public:
    applied_migration();

    int64_t database_id() const;
    const std::string& migration_name() const;
    const std::string& checksum() const;
    std::chrono::time_point<std::chrono::system_clock> applied_on() const;

    applied_migration& database_id(int64_t id);
    applied_migration& migration_name(std::string name);
    applied_migration& checksum(std::string checksum);
    applied_migration& applied_on(std::chrono::time_point<std::chrono::system_clock> applied_on);

private:
    int64_t _database_id;
    std::string _migration_name;
    std::string _checksum;
    std::chrono::time_point<std::chrono::system_clock> _applied_on;
};

}
