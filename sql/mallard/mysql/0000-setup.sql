CREATE TABLE mallard_version(
  version     bigint    UNSIGNED  NOT NULL AUTO_INCREMENT,
  applied_on  datetime            NOT NULL,

  PRIMARY KEY (version)
);