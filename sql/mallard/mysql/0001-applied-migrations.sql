CREATE TABLE applied_migrations(
  id            bigint    UNSIGNED  NOT NULL  AUTO_INCREMENT,
  name          text                NOT NULL,
  description   text,
  requires      text,
  checksum      blob                NOT NULL,
  script_text   text                NOT NULL,
  applied_on    datetime            NOT NULL  DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (id)
)