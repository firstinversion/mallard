#include <catch2/catch.hpp>

#include <mallard/testers/postgre_tester.h>
#include <mallard/test_parser.h>
#include "../temporary_postgres_database.hpp"

SCENARIO("Testing of Postgre database", "[tester]") {

    GIVEN("An empty database.") {
        temporary_postgre_database tempdb;
        mallard::postgre_tester tester(tempdb.session());
        mallard::test_plan plan;
        mallard::parse_test_from_file("./test/resource/test_files/postgre/base_tests.sql", std::back_inserter(plan));

        tester.apply(plan);
    }
}
