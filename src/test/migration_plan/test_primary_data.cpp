#include <catch2/catch.hpp>

#include <mallard/migration_plan.h>
#include <mallard/migration_parser.h>

SCENARIO("Migration Plan data manipulation", "[migration_plan]") {
    GIVEN("A valid chain of dependent migrations.") {
        auto migrations = mallard::parse_migration_from_file<std::vector<mallard::migration>>(
                "./test/resource/migration_files/chain.sql");
        mallard::migration_plan plan(migrations);
        const auto& plan_ref = plan;

        plan.validate();
        REQUIRE(plan.valid());

        WHEN("Accessing an element.") {
            REQUIRE(plan[1].name() == migrations[1].name());
            REQUIRE(!plan.valid());
        }

        WHEN("Accessing from element (const ref).") {
            REQUIRE(plan_ref[1].name() == migrations[1].name());
            REQUIRE(plan_ref.valid());
        }

        WHEN("Checked accessing an element.") {
            REQUIRE(plan.at(1).name() == migrations.at(1).name());
            REQUIRE(!plan.valid());
            REQUIRE_THROWS(plan.at(-1));
            REQUIRE_THROWS(plan.at(4));
        }

        WHEN("Checked accessing an element (const ref).") {
            REQUIRE(plan_ref.at(1).name() == migrations.at(1).name());
            REQUIRE(plan_ref.valid());
            REQUIRE_THROWS(plan_ref.at(-1));
            REQUIRE_THROWS(plan_ref.at(4));
        }

        WHEN("Accessing the front element.") {
            REQUIRE(plan.front().name() == migrations.front().name());
            REQUIRE(!plan.valid());
        }

        WHEN("Accessing the front element (const ref).") {
            REQUIRE(plan_ref.front().name() == migrations.front().name());
            REQUIRE(plan_ref.valid());
        }

        WHEN("Accessing the back element.") {
            REQUIRE(plan.back().name() == migrations.back().name());
            REQUIRE(!plan.valid());
        }

        WHEN("Accessing the back element (const ref).") {
            REQUIRE(plan_ref.back().name() == migrations.back().name());
            REQUIRE(plan_ref.valid());
        }

        WHEN("Accessing the migration data.") {
            REQUIRE(plan.data()[0].name()
                    == migrations.data()[0].name()); // NOLINT(readability-simplify-subscript-expr)
            REQUIRE(!plan.valid());
        }

        WHEN("Accessing the migration data (const ref).") {
            REQUIRE(plan_ref.data()[0].name()
                    == migrations.data()[0].name()); // NOLINT(readability-simplify-subscript-expr)
            REQUIRE(plan_ref.valid());
        }

        WHEN("Getting begin iterator.") {
            REQUIRE((*plan.begin()).name() == (*migrations.begin()).name());
            REQUIRE(!plan.valid());
        }

        WHEN("Getting begin iterator (const ref).") {
            REQUIRE((*plan_ref.begin()).name() == (*migrations.begin()).name());
            REQUIRE(plan_ref.valid());
        }

        WHEN("Getting cbegin iterator.") {
            REQUIRE((*plan.cbegin()).name() == (*migrations.cbegin()).name());
            REQUIRE(plan.valid());
        }

        WHEN("Getting end iterator.") {
            REQUIRE(plan.begin() + plan.size() == plan.end());
            REQUIRE(!plan.valid());
        }

        WHEN("Getting end iterator (const ref).") {
            REQUIRE(plan_ref.begin() + plan_ref.size() == plan_ref.end());
            REQUIRE(plan_ref.valid());
        }

        WHEN("Getting cend iterator.") {
            REQUIRE(plan.cbegin() + plan.size() == plan.cend());
            REQUIRE(plan.valid());
        }

        WHEN("Getting rbegin iterator.") {
            REQUIRE((*plan.rbegin()).name() == (*migrations.rbegin()).name());
            REQUIRE(!plan.valid());
        }

        WHEN("Getting rbegin iterator (const ref).") {
            REQUIRE((*plan_ref.rbegin()).name() == (*migrations.crbegin()).name());
            REQUIRE(plan.valid());
        }

        WHEN("Getting crbegin iterator.") {
            REQUIRE((*plan.crbegin()).name() == (*migrations.crbegin()).name());
            REQUIRE(plan.valid());
        }

        WHEN("Getting rend iterator.") {
            REQUIRE(plan.rbegin() + plan.size() == plan.rend());
            REQUIRE(!plan.valid());
        }

        WHEN("Getting rend iterator (const ref).") {
            REQUIRE(plan_ref.rbegin() + plan_ref.size() == plan_ref.rend());
            REQUIRE(plan.valid());
        }

        WHEN("Getting crend iterator.") {
            REQUIRE(plan.crbegin() + plan.size() == plan.crend());
            REQUIRE(plan.valid());
        }

        WHEN("Getting the emptiness of the plan.") {
            REQUIRE(plan.empty() == migrations.empty());
            REQUIRE(plan.valid());
        }

        WHEN("Getting the size of the plan.") {
            REQUIRE(plan.size() == migrations.size());
            REQUIRE(plan.valid());
        }

        WHEN("Getting the max_size of the plan.") {
            REQUIRE(plan.max_size() == migrations.max_size());
            REQUIRE(plan.valid());
        }

        WHEN("Reserving space on the plan.") {
            plan.reserve(100);
            REQUIRE(plan.capacity() == 100);
            REQUIRE(plan.valid());
        }

        WHEN("Getting the capacity of the plan.") {
            plan.capacity();
            REQUIRE(plan.valid());
        }

        WHEN("Shrinking the capacity of the plan.") {
            plan.reserve(100);
            plan.shrink_to_fit();
            REQUIRE(plan.valid());
        }

        WHEN("Clearing the plan.") {
            plan.clear();
            REQUIRE(plan.empty());
            REQUIRE(!plan.valid());
        }

        WHEN("Inserting to front.") {
            mallard::migration m;
            m.name("test");
            size_t prior_size = plan.size();

            plan.insert(plan.begin(), m);
            REQUIRE(plan.size() == prior_size + 1);
            REQUIRE(plan.at(0).name() == "test");
            REQUIRE(!plan.valid());
        }

        WHEN("Inserting many copies of a value.") {
            mallard::migration m;
            m.name("test");
            size_t prior_size = plan.size();

            plan.insert(plan.begin(), 2, m);
            REQUIRE(plan.size() == prior_size + 2);
            REQUIRE(plan.at(0).name() == "test");
            REQUIRE(plan.at(1).name() == "test");
            REQUIRE(!plan.valid());
        }

        WHEN("Inserting from an input iterator.") {
            std::vector<mallard::migration> additional(2);
            additional[0].name("test1");
            additional[1].name("test2");
            size_t prior_size = plan.size();

            plan.insert(plan.begin(), additional.begin(), additional.end());
            REQUIRE(plan.at(0).name() == "test1");
            REQUIRE(plan.at(1).name() == "test2");
            REQUIRE(plan.size() == prior_size + 2);
            REQUIRE(!plan.valid());
        }

        WHEN("Inserting from initializer list.") {
            mallard::migration m1;
            m1.name("test1");
            mallard::migration m2;
            m2.name("test2");
            size_t prior_size = plan.size();

            plan.insert(plan.begin(), {m1, m2});
            REQUIRE(plan.at(0).name() == "test1");
            REQUIRE(plan.at(1).name() == "test2");
            REQUIRE(plan.size() == prior_size + 2);
            REQUIRE(!plan.valid());
        }

        WHEN("Emplacing a migration.") {
            size_t prior_size = plan.size();

            plan.emplace(plan.begin(), "test");
            REQUIRE(plan.front().name() == "test");
            REQUIRE(plan.size() == prior_size + 1);
            REQUIRE(!plan.valid());
        }

        WHEN("Erasing a single value.") {
            size_t prior_size = plan.size();

            plan.erase(plan.begin() + 1);
            REQUIRE(plan.at(0).name() == "schema");
            REQUIRE(plan.at(1).name() == "table/phone");
            REQUIRE(plan.size() == prior_size - 1);
            REQUIRE(!plan.valid());
        }

        WHEN("Erasing a range of values.") {
            plan.emplace(plan.end(), "table/address");
            size_t prior_size = plan.size();

            plan.erase(plan.begin() + 1, plan.end() - 1);
            REQUIRE(plan.at(0).name() == "schema");
            REQUIRE(plan.at(1).name() == "table/address");
            REQUIRE(plan.size() == prior_size - 2);
            REQUIRE(!plan.valid());
        }

        WHEN("Pushing a value the back.") {
            mallard::migration m("test");
            size_t prior_size = plan.size();

            plan.push_back(m);
            REQUIRE(plan.back().name() == "test");
            REQUIRE(plan.size() == prior_size + 1);
            REQUIRE(!plan.valid());
        }

        WHEN("Emplacing a value to the back.") {
            size_t prior_size = plan.size();

            plan.emplace_back("test");
            REQUIRE(plan.back().name() == "test");
            REQUIRE(plan.size() == prior_size + 1);
            REQUIRE(!plan.valid());
        }

        WHEN("Poping an element off the back.") {
            size_t prior_size = plan.size();

            plan.pop_back();
            REQUIRE(plan.back().name() == "table/person");
            REQUIRE(plan.size() == prior_size - 1);
            REQUIRE(!plan.valid());
        }

        WHEN("Resizing to a smaller size.") {
            size_t prior_size = plan.size();

            plan.resize(prior_size - 1);
            REQUIRE(plan.back().name() == "table/person");
            REQUIRE(plan.size() == prior_size - 1);
            REQUIRE(!plan.valid());
        }

        WHEN("Resizing to a larger size") {
            size_t prior_size = plan.size();

            plan.resize(prior_size + 1);
            REQUIRE(plan.back().name().empty());
            REQUIRE(plan.size() == prior_size + 1);
            REQUIRE(!plan.valid());
        }

        WHEN("Resizing to a larger size with specified value.") {
            size_t prior_size = plan.size();

            plan.resize(prior_size + 1, mallard::migration("test"));
            REQUIRE(plan.back().name() == "test");
            REQUIRE(plan.size() == prior_size + 1);
            REQUIRE(!plan.valid());
        }

        WHEN("Swapping the contests of two plans.") {
            mallard::migration_plan alternate_plan = {mallard::migration("test1"), mallard::migration("test2")};

            plan.swap(alternate_plan);
            REQUIRE(plan.size() == 2);
            REQUIRE(plan.front().name() == "test1");
            REQUIRE(plan.back().name() == "test2");
            REQUIRE(alternate_plan.front().name() == "schema");
            REQUIRE(!plan.valid());
            REQUIRE(!alternate_plan.valid());
        }
    }
}
