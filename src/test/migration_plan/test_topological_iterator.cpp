#include <catch2/catch.hpp>

#include <algorithm>
#include <random>
#include <mallard/migration_parser.h>
#include <mallard/migration_plan.h>

SCENARIO("Topological iterator for migration_plan.", "[migration_plan]") {

    GIVEN("A chain of dependent migrations.") {
        mallard::migration_plan plan(mallard::parse_migration_from_file("./test/resource/migration_files/chain.sql"));
        REQUIRE(plan.size() == 3);

        WHEN("Default ordered.") {
            plan.validate();
            std::vector<mallard::migration> migrations;
            std::copy(plan.tbegin(), plan.tend(), std::back_inserter(migrations));
            REQUIRE(migrations.size() == 3);
            REQUIRE(migrations[0].name() == "schema");
            REQUIRE(migrations[1].name() == "table/person");
            REQUIRE(migrations[2].name() == "table/phone");
        }

        WHEN("Reverse ordered.") {
            std::reverse(plan.begin(), plan.end());
            plan.validate();

            std::vector<mallard::migration> migrations;
            std::copy(plan.tbegin(), plan.tend(), std::back_inserter(migrations));
            REQUIRE(migrations.size() == 3);
            REQUIRE(migrations[0].name() == "schema");
            REQUIRE(migrations[1].name() == "table/person");
            REQUIRE(migrations[2].name() == "table/phone");
        }

        WHEN("Random ordered.") {
            std::random_device rd;
            std::mt19937 g(rd());
            std::shuffle(plan.begin(), plan.end(), g);
            plan.validate();

            std::vector<mallard::migration> migrations;
            std::copy(plan.tbegin(), plan.tend(), std::back_inserter(migrations));
            REQUIRE(migrations.size() == 3);
            REQUIRE(migrations[0].name() == "schema");
            REQUIRE(migrations[1].name() == "table/person");
            REQUIRE(migrations[2].name() == "table/phone");
        }
    }
}
