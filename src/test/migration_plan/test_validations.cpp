#include <catch2/catch.hpp>

#include <mallard/migration_plan.h>
#include <mallard/migration.h>
#include <mallard/migration_parser.h>

SCENARIO("validation migration plans", "[migration_plan]") {
    GIVEN("A chain of valid migrations.") {
        mallard::migration_plan plan = mallard::parse_migration_from_file("./test/resource/migration_files/chain.sql");
        REQUIRE(plan.validate().empty());
    }

    GIVEN("Overloaded migration names.") {
        mallard::migration_plan plan = mallard::parse_migration_from_file(
                "./test/resource/migration_files/overloaded_names.sql");
        mallard::errors e = plan.validate();
        REQUIRE(e.size() == 1);
        REQUIRE(dynamic_cast<mallard::name_overlap_error*>(e[0].get()));
    }

    GIVEN("A migration with missing dependency.") {
        mallard::migration_plan plan =
                mallard::parse_migration_from_file("./test/resource/migration_files/missing_dependency.sql");
        mallard::errors e = plan.validate();
        REQUIRE(e.size() == 1);
        REQUIRE(dynamic_cast<mallard::missing_dependency_error*>(e[0].get()));
    }

    GIVEN("A migration with a dependency cycle.") {
        mallard::migration_plan plan =
                mallard::parse_migration_from_file("./test/resource/migration_files/dependency_cycle.sql");
        mallard::errors e = plan.validate();
        REQUIRE(e.size() == 1);
        REQUIRE(dynamic_cast<mallard::dependency_cycle_error*>(e[0].get()));
    }
}
