#pragma once

#include <random>
#include <pqxx/pqxx>
#include <fmt/format.h>

class temporary_postgre_database {
public:
    temporary_postgre_database();
    temporary_postgre_database(std::string user, std::string password, std::string hostname, std::string port);
    temporary_postgre_database(std::string user, std::string password, std::string hostname, std::string port,
                               std::string name);
    ~temporary_postgre_database();

    const std::string& user();
    const std::string& password();
    const std::string& hostname();
    const std::string& port();
    const std::string& name();

    pqxx::connection& session();

private:
    void setup();
    std::string generate_name();

    std::string _user;
    std::string _password;
    std::string _hostname;
    std::string _port;
    std::string _name;

    std::unique_ptr<pqxx::connection> _session;
};
