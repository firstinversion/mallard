#pragma once

#include <string>
#include <boost/filesystem.hpp>
#include <soci/session.h>

class temporary_sqlite_database {
public:
    explicit temporary_sqlite_database(std::string file_path);
    ~temporary_sqlite_database();

    const std::string& file_path();
    soci::session& session();

private:
    std::string _file_path;
    soci::session _session;
};

temporary_sqlite_database::temporary_sqlite_database(std::string file_path)
        : _file_path(std::move(file_path)), _session("sqlite", _file_path) {}

temporary_sqlite_database::~temporary_sqlite_database() {
    boost::filesystem::remove(_file_path);
}

const std::string& temporary_sqlite_database::file_path() {
    return _file_path;
}

soci::session& temporary_sqlite_database::session() {
    return _session;
}
