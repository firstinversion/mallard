#include <catch2/catch.hpp>

#include <set>
#include <algorithm>
#include <iterator>
#include <mallard/migration_plan.h>
#include <mallard/migration_parser.h>
#include <mallard/applicators/sqlite_applicator.h>
#include <SQLiteCpp/SQLiteCpp.h>

bool sqlite_table_exists(SQLite::Database& session, const std::string& table_name) {
    SQLite::Statement query(session, "SELECT count(name) FROM sqlite_master WHERE name = :name;");
    query.bind(":name", table_name);
    query.executeStep();
    return query.getColumn(0).getInt64() > 0;
}

SCENARIO("Application of migrations to SQLite", "[applicator]") {

    GIVEN("An empty database.") {

        WHEN("Using implicit initialization.") {
            mallard::sqlite_applicator applicator(":memory:");
            REQUIRE(applicator.find_version() == 1);
        }

        WHEN("Using explicit initialization.") {
            mallard::sqlite_applicator applicator(":memory:", false);
            REQUIRE(applicator.find_version() == 0);
            applicator.initialize();
            REQUIRE(applicator.find_version() == 1);
        }
    }

    GIVEN("A chain of dependent migrations, and empty database.") {
        mallard::migration_plan plan = mallard::parse_migration_from_file(
                "./test/resource/migration_files/sqlite/chain.sql");
        plan.validate();

        mallard::sqlite_applicator applicator(":memory:");
        applicator.apply(plan);
        REQUIRE(sqlite_table_exists(applicator.session(), "person"));
        REQUIRE(sqlite_table_exists(applicator.session(), "phone"));

        WHEN("Finding applied migrations.") {
            std::vector<mallard::applied_migration> applied;
            applicator.find_applied_migrations(std::back_inserter(applied));

            REQUIRE(applied.at(0).migration_name() == "table/person");
            REQUIRE(applied.at(1).migration_name() == "table/phone");
        }

        WHEN("Applying a second time does nothing.") {
            applicator.apply(plan);
            REQUIRE(sqlite_table_exists(applicator.session(), "person"));
            REQUIRE(sqlite_table_exists(applicator.session(), "phone"));
        }
    }

    GIVEN("Two versions of a plan, applied at different times.") {
        mallard::migration_plan plan;
        mallard::parse_migration_from_file("./test/resource/migration_files/sqlite/partial_application/part1.sql",
                                           std::back_inserter(plan));
        REQUIRE(plan.validate().empty());

        mallard::sqlite_applicator applicator(":memory:");
        applicator.apply(plan);

        REQUIRE(sqlite_table_exists(applicator.session(), "person"));
        REQUIRE(sqlite_table_exists(applicator.session(), "phone"));
        REQUIRE(!sqlite_table_exists(applicator.session(), "address"));
        REQUIRE(!sqlite_table_exists(applicator.session(), "email"));

        mallard::parse_migration_from_file("./test/resource/migration_files/sqlite/partial_application/part2.sql",
                                           std::back_inserter(plan));
        REQUIRE(plan.validate().empty());
        applicator.apply(plan);

        REQUIRE(sqlite_table_exists(applicator.session(), "person"));
        REQUIRE(sqlite_table_exists(applicator.session(), "phone"));
        REQUIRE(sqlite_table_exists(applicator.session(), "address"));
        REQUIRE(sqlite_table_exists(applicator.session(), "email"));
    }
}
