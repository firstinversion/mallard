#include <catch2/catch.hpp>

#include <set>
#include <algorithm>
#include <iterator>
#include <mallard/migration_plan.h>
#include <mallard/migration_parser.h>
#include <mallard/applicators/postgre_applicator.h>
#include <pqxx/pqxx>
#include "../temporary_postgres_database.hpp"

bool postgre_table_exists(pqxx::connection& session, const std::string& schema_name, const std::string& table_name) {
    pqxx::work tx(session);
    pqxx::row count = tx.exec_params1(
            "SELECT count(table_name)\n"
            "FROM information_schema.tables\n"
            "WHERE table_schema = $1 AND table_name = $2;",
            schema_name, table_name);
    return count[0].as<int64_t>() == 1;
}


SCENARIO("Application of migrations to Postgre", "[applicator]") {

    GIVEN("An empty database.") {

        WHEN("Using implicit initialization.") {
            temporary_postgre_database tempdb;
            mallard::postgre_applicator applicator(tempdb.session());
            REQUIRE(applicator.find_version() == 1);
        }

        WHEN("Using explicit initialization.") {
            temporary_postgre_database tempdb;
            mallard::postgre_applicator applicator(tempdb.session(), false);
            REQUIRE(applicator.find_version() == 0);
            applicator.initialize();
            REQUIRE(applicator.find_version() == 1);
        }
    }

    GIVEN("A chain of dependent migrations, and empty database.") {
        temporary_postgre_database tempdb;
        mallard::migration_plan plan = mallard::parse_migration_from_file(
                "./test/resource/migration_files/postgre/chain.sql");
        plan.validate();

        mallard::postgre_applicator applicator(tempdb.session());
        applicator.apply(plan);
        REQUIRE(postgre_table_exists(tempdb.session(), "public", "person"));
        REQUIRE(postgre_table_exists(tempdb.session(), "public", "phone"));

        WHEN("Finding applied migrations.") {
            std::vector<mallard::applied_migration> applied;
            applicator.find_applied_migrations(std::back_inserter(applied));

            REQUIRE(applied.at(0).migration_name() == "table/person");
            REQUIRE(applied.at(1).migration_name() == "table/phone");
        }

        WHEN("Applying a second time does nothing.") {
            applicator.apply(plan);
            REQUIRE(postgre_table_exists(tempdb.session(), "public", "person"));
            REQUIRE(postgre_table_exists(tempdb.session(), "public", "phone"));
        }
    }

    GIVEN("Two versions of a plan, applied at different times.") {
        temporary_postgre_database tempdb;
        mallard::migration_plan plan;
        mallard::parse_migration_from_file("./test/resource/migration_files/postgre/partial_application/part1.sql",
                                           std::back_inserter(plan));
        REQUIRE(plan.validate().empty());

        mallard::postgre_applicator applicator(tempdb.session());
        applicator.apply(plan);

        REQUIRE(postgre_table_exists(tempdb.session(), "public", "person"));
        REQUIRE(postgre_table_exists(tempdb.session(), "public", "phone"));
        REQUIRE(!postgre_table_exists(tempdb.session(), "public", "address"));
        REQUIRE(!postgre_table_exists(tempdb.session(), "public", "email"));

        mallard::parse_migration_from_file("./test/resource/migration_files/postgre/partial_application/part2.sql",
                                           std::back_inserter(plan));
        REQUIRE(plan.validate().empty());
        applicator.apply(plan);

        REQUIRE(postgre_table_exists(tempdb.session(), "public", "person"));
        REQUIRE(postgre_table_exists(tempdb.session(), "public", "phone"));
        REQUIRE(postgre_table_exists(tempdb.session(), "public", "address"));
        REQUIRE(postgre_table_exists(tempdb.session(), "public", "email"));
    }
}
