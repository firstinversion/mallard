#include <catch2/catch.hpp>

#include <mallard/grammar/test.h>

namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;

SCENARIO("component testing test grammar", "[parser]") {

    mallard::test_grammar<std::string::const_iterator> grammar;
    mallard::mallard_temp_test_skipper<std::string::const_iterator> skipper;

    GIVEN("a word") {
        std::string input("word");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.word_rule, skipper, output));
        REQUIRE(output == "word");
    }

    GIVEN("two words") {
        std::string input("word one");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.word_rule, skipper, output));
        REQUIRE(output == "word");
    }

    GIVEN("two word delimited by comma") {
        std::string input("word,one");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.word_rule, skipper, output));
        REQUIRE(output == "word");
    }

    GIVEN ("a name field") {
        std::string input("name: nameval");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.name_rule, skipper, output));
        REQUIRE(output == "nameval");
    }

    GIVEN("a name field trailed by a quote") {
        std::string input("name: nameval,");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.name_rule, skipper, output));
        REQUIRE(output == "nameval");
    }

    GIVEN("a description field") {
        std::string input("description: descriptionval");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.description_rule, skipper, output));
        REQUIRE(output == "descriptionval");
    }

    GIVEN("a description field with spaces") {
        std::string input("description: description value");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.description_rule, skipper, output));
        REQUIRE(output == "description value");
    }

    GIVEN("a description field with spaces and trailed by comma") {
        std::string input("description: description value,");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.description_rule, skipper, output));
        REQUIRE(output == "description value");
    }

    GIVEN("a header") {
        std::string input("#!test name: name, description: description;");
        mallard::test output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.test_rule, skipper, output));
        REQUIRE(output.name() == "name");
        REQUIRE(output.description() == "description");
    }

    GIVEN("a multiline header") {
        std::string input("#!test name: name,\n"
                          "description: description;");
        mallard::test output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.test_rule, skipper, output));
        REQUIRE(output.name() == "name");
        REQUIRE(output.description() == "description");
    }

    GIVEN("a multiline header in comments") {
        std::string input("-- #!test name: name,\n"
                          "-- description: description;");
        mallard::test output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.test_rule, skipper, output));
        REQUIRE(output.name() == "name");
        REQUIRE(output.description() == "description");
    }

    GIVEN("a test") {
        std::string input("-- #!test name: name, description: description;SELECT something FROM somewhere;");
        mallard::test output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.test_rule, skipper, output));
        REQUIRE(output.name() == "name");
        REQUIRE(output.description() == "description");
        REQUIRE(output.script_text() == "SELECT something FROM somewhere;");
    }

    GIVEN("a test over multiple lines") {
        std::string input("-- #!test\n"
                          "-- name: name,\n"
                          "-- description: description;\n"
                          "SELECT something FROM somewhere;");
        mallard::test output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.test_rule, skipper, output));
        REQUIRE(output.name() == "name");
        REQUIRE(output.description() == "description");
        REQUIRE(output.script_text() == "SELECT something FROM somewhere;");
    }

    GIVEN("body") {
        std::string input("SELECT something FROM somewhere;");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.until_next_rule, skipper, output));
        REQUIRE(output == "SELECT something FROM somewhere;");
    }

    GIVEN("body with next") {
        std::string input("SELECT something FROM somewhere;#!test name");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.until_next_rule, skipper, output));
        REQUIRE(output == "SELECT something FROM somewhere;");
    }

    GIVEN("body with next in comment") {
        std::string input("SELECT something FROM somewhere; -- #!test");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.until_next_rule, skipper, output));
        REQUIRE(output == "SELECT something FROM somewhere; -- ");
    }

    GIVEN("body with next in next line comment") {
        std::string input("SELECT something FROM somewhere;\n"
                          "-- #!test");
        std::string output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.until_next_rule, skipper, output));
        REQUIRE(output == "SELECT something FROM somewhere;\n-- ");
    }

}
