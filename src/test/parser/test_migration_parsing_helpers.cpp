#include <catch2/catch.hpp>
#include <mallard/migration.h>
#include <mallard/migration_plan.h>
#include <mallard/migration_parser.h>

SCENARIO("parser migrations from individual files", "[parser]") {
    GIVEN("A string with migrations.") {
        std::string input("#!migration\n"
                          "name: table/person;\n"
                          "CREATE TABLE person;\n"
                          "#!migration\n"
                          "name: table/phone,\n"
                          "requires: [table/person];\n"
                          "CREATE TABLE phone;\n");
        mallard::migration_plan output = mallard::parse_migration_from_string(input);
        REQUIRE(output.at(0).name() == "table/person");
        REQUIRE(output.at(1).name() == "table/phone");
    }

    GIVEN("A file that does not exist.") {
        std::string badpath = "./somewhere/that/isnt/real/script.mallard";

        bool gotException = false;
        try {
            mallard::parse_migration_from_file(badpath);
        }
        catch (const std::invalid_argument& e) {
            gotException = true;
        }

        REQUIRE(gotException);
    }

    GIVEN("A file path containing a single schema migration.") {
        std::string path = "./test/resource/migration_files/single.sql";
        mallard::migration_plan migrations = mallard::parse_migration_from_file(path);
        REQUIRE(migrations.size() == 1);
        REQUIRE(migrations.at(0).name() == "schema");
    }

    GIVEN("A file path containing a single schema migration, with trailing newline.") {
        std::string path = "./test/resource/migration_files/single_newline.sql";
        mallard::migration_plan migrations = mallard::parse_migration_from_file(path);
        REQUIRE(migrations.size() == 1);
        REQUIRE(migrations.at(0).name() == "schema");
        REQUIRE(migrations.at(0).script_text() == "CREATE SCHEMA contact;");
    }

    GIVEN("A file of migrations and an empty vector.") {
        std::string path = "./test/resource/migration_files/chain.sql";
        std::vector<mallard::migration> migrations;
        mallard::parse_migration_from_file(path, std::back_inserter(migrations));
        REQUIRE(migrations.size() == 3);
        REQUIRE(migrations.at(0).name() == "schema");
        REQUIRE(migrations.at(2).name() == "table/phone");
    }

    GIVEN("A file of migrations and an empty migration_plan.") {
        std::string path = "./test/resource/migration_files/chain.sql";
        mallard::migration_plan plan;
        mallard::parse_migration_from_file(path, std::back_inserter(plan));
        REQUIRE(plan.size() == 3);
        REQUIRE(plan.at(0).name() == "schema");
        REQUIRE(plan.at(2).name() == "table/phone");
    }

    GIVEN("A file of migrations with custom container construction.") {
        std::string path = "./test/resource/migration_files/chain.sql";
        auto migrations = mallard::parse_migration_from_file<std::vector<mallard::migration>>(path);
        REQUIRE(migrations.size() == 3);
        REQUIRE(migrations.at(0).name() == "schema");
        REQUIRE(migrations.at(2).name() == "table/phone");
    }
}
