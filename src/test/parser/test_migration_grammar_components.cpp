#include <catch2/catch.hpp>

#include <mallard/migration_parser.h>

namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;

SCENARIO("component testing header_rule parser", "[parser]") {

    mallard::migration_grammar<std::string::const_iterator> grammar;
    mallard::mallard_default_skipper<std::string::const_iterator> skipper;

//    GIVEN("A quote string.") {
//        std::string input("\"this\"");
//        std::string output;
//        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.quote_string_rule, skipper, output));
//        REQUIRE(output == "this");
//    }
//
//    GIVEN("A list of quote string.") {
//        std::string input(R"(["this","is","a","sentence"])");
//        std::vector<std::string> output;
//        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.quote_string_list_rule, skipper, output));
//        REQUIRE(output == std::vector<std::string>({"this", "is", "a", "sentence"}));
//    }
//
//    GIVEN("A field_rule name.") {
//        std::string input("description");
//        std::string output;
//        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.field_name_rule, skipper, output));
//        REQUIRE(output == "description");
//    }
//
//    GIVEN("A string field_rule value.") {
//        std::string input("\"table/person\"");
//        mallard::field_value output;
//        std::string* outputAs;
//        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.field_value_rule, skipper, output));
//        outputAs = boost::get<std::string>(&output);
//        REQUIRE(*outputAs == "table/person");
//    }
//
//    GIVEN("A string list field_rule value.") {
//        std::string input(R"(["this","is","a","sentence"])");
//        mallard::field_value output;
//        std::vector<std::string>* outputAs;
//        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.field_value_rule, skipper, output));
//        outputAs = boost::get<std::vector<std::string>>(&output);
//        REQUIRE(*outputAs == std::vector<std::string>({"this", "is", "a", "sentence"}));
//    }
//
//    GIVEN("A string field_rule.") {
//        std::string input("name: \"table/person\"");
//        mallard::field output;
//        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.field_rule, skipper, output));
//        REQUIRE(output.first == "name");
//
//        std::string* outputValueAs = boost::get<std::string>(&output.second);
//        REQUIRE(*outputValueAs == "table/person");
//    }
//
//    GIVEN("A string list field_rule.") {
//        std::string input(R"(names: ["this","is","a","sentence"])");
//        mallard::field output;
//        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.field_rule, skipper, output));
//        REQUIRE(output.first == "names");
//
//        std::vector<std::string>* outputValueAs = boost::get<std::vector<std::string>>(&output.second);
//        REQUIRE(*outputValueAs == std::vector<std::string>({"this", "is", "a", "sentence"}));
//    }
//
//    GIVEN("Multiple fields_rule.") {
//        std::string input(R"(first:"john",last:"doe")");
//        mallard::fields output;
//        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.fields_rule, skipper, output));
//        REQUIRE(*(boost::get<std::string>(&output["first"])) == "john");
//        REQUIRE(*(boost::get<std::string>(&output["last"])) == "doe");
//    }
//
//    GIVEN("A header_rule.") {
//        std::string input("#!migration\n"
//                          "name: \"table/phone\",\n"
//                          "requires: [\"table/person\"];\n");
//        mallard::fields output;
//        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.header_rule, skipper, output));
//        REQUIRE(*(boost::get<std::string>(&output["name"])) == "table/phone");
//        REQUIRE(*(boost::get<std::vector<std::string>>(&output["requires"]))
//                == std::vector<std::string>({"table/person"}));
//    }
//
//    GIVEN("A body_rule.") {
//        std::string input("CREATE TABLE person;");
//        std::string output;
//        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.body_rule, skipper, output));
//        REQUIRE(output == "CREATE TABLE person;");
//    }
//
//    GIVEN("A body_rule with partial header_rule.") {
//        std::string input("CREATE TABLE person;#!");
//        std::string output;
//        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.body_rule, skipper, output));
//        REQUIRE(output == "CREATE TABLE person;");
//    }

    GIVEN("an empty list") {
        std::string input("[]");
        std::vector<std::string> output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.empty_list_rule, skipper, output));
        REQUIRE(output.empty());
    }

    GIVEN("a list of one element") {
        std::string input("[element]");
        std::vector<std::string> output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.list_rule, skipper, output));
        REQUIRE(output.size() == 1);
        REQUIRE(output[0] == "element");
    }

    GIVEN("a list of two elements") {
        std::string input("[one,two]");
        std::vector<std::string> output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.list_rule, skipper, output));
        REQUIRE(output.size() == 2);
        REQUIRE(output[0] == "one");
        REQUIRE(output[1] == "two");
    }

    GIVEN("an empty list with space") {
        std::string input("[ ]");
        std::vector<std::string> output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.empty_list_rule, skipper, output));
        REQUIRE(output.empty());
    }

    GIVEN("a list of one element with spaces") {
        std::string input("[ element ]");
        std::vector<std::string> output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.list_rule, skipper, output));
        REQUIRE(output.size() == 1);
        REQUIRE(output[0] == "element");
    }

    GIVEN("a list of two elements with spaces") {
        std::string input("[ one, two ]");
        std::vector<std::string> output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.list_rule, skipper, output));
        REQUIRE(output.size() == 2);
        REQUIRE(output[0] == "one");
        REQUIRE(output[1] == "two");
    }

    GIVEN("a requires field") {
        std::string input("requires: [table/phone]");
        std::vector<std::string> output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.requires_rule, skipper, output));
        REQUIRE(output.size() == 1);
        REQUIRE(output[0] == "table/phone");
    }

    GIVEN("a requires field with multiple dependencies") {
        std::string input("requires: [table/phone, table/address]");
        std::vector<std::string> output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.requires_rule, skipper, output));
        REQUIRE(output.size() == 2);
        REQUIRE(output[0] == "table/phone");
        REQUIRE(output[1] == "table/address");
    }

    GIVEN("A migration.") {
        std::string input("-- #!migration\n"
                          "-- name: table/phone,\n"
                          "-- requires: [table/person];\n"
                          "CREATE TABLE phone;");
        mallard::migration output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.migration_rule, skipper, output));
        REQUIRE(output.name() == "table/phone");
        REQUIRE(output.requires_().size() == 1);
        REQUIRE(output.requires_()[0] == "table/person");
        REQUIRE(output.script_text() == "CREATE TABLE phone;");
    }

    GIVEN("Two migrations.") {
        std::string input("-- #!migration\n"
                          "-- name: table/person;\n"
                          "CREATE TABLE person;\n"
                          "-- #!migration\n"
                          "-- name: table/phone,\n"
                          "-- requires: [table/person];\n"
                          "CREATE TABLE phone;\n");
        std::vector<mallard::migration> output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.migration_list_rule, skipper, output));
        REQUIRE(output.size() == 2);
        REQUIRE(output.at(0).name() == "table/person");
        REQUIRE(output.at(1).name() == "table/phone");
    }

    GIVEN("migration in comments.") {
        std::string input("-- #!migration\n"
                          "-- name: table/phone,\n"
                          "-- requires: [table/person];\n"
                          "CREATE TABLE phone;");
        mallard::migration output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.migration_rule, skipper, output));
        REQUIRE(output.name() == "table/phone");
        REQUIRE(output.script_text() == "CREATE TABLE phone;");
    }

    GIVEN("migration with internal comment.") {
        std::string input("-- #!migration\n"
                          "-- name: tables;\n"
                          "-- Create the person table.\n"
                          "CREATE TABLE person;\n"
                          "-- Create the phone table.\n"
                          "CREATE TABLE phone;\n"
                          "-- This migration does things.");
        mallard::migration output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.migration_rule, skipper, output));
        REQUIRE(output.script_text() == "-- Create the person table.\n"
                                        "CREATE TABLE person;\n"
                                        "-- Create the phone table.\n"
                                        "CREATE TABLE phone;\n"
                                        "-- This migration does things.");
    }

    GIVEN("migration with excess whitespace.") {
        std::string input("-- #!migration\n"
                          "-- name: table/phone,\n"
                          "-- requires: [table/person];\n"
                          "\n"
                          "CREATE TABLE phone;\n"
                          "\n");
        mallard::migration output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.migration_rule, skipper, output));
        REQUIRE(output.script_text() == "CREATE TABLE phone;");
    }

    GIVEN("migration with excess whitespace, before partial header_rule.") {
        std::string input("-- #!migration\n"
                          "-- name: table/phone,\n"
                          "-- requires: [table/person];\n"
                          "\n"
                          "CREATE TABLE phone;\n"
                          "\n"
                          "-- #!");
        mallard::migration output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.migration_rule, skipper, output));
        REQUIRE(output.script_text() == "CREATE TABLE phone;");
    }

    GIVEN("migration with content before migration tag.") {
        std::string input("-- Comments happening above migration. "
                          "-- #!migration\n"
                          "-- name: table/phone,\n"
                          "-- requires: [table/person];\n"
                          "CREATE TABLE phone;");
        mallard::migration output;
        REQUIRE(qi::phrase_parse(input.cbegin(), input.cend(), grammar.migration_rule, skipper, output));
        REQUIRE(output.name() == "table/phone");
        REQUIRE(output.requires_().size()  == 1);
        REQUIRE(output.requires_()[0] == "table/person");
    }
}
