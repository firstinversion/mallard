#include "temporary_postgres_database.hpp"

std::string get_env(const char* env, std::string def) {
    char* tmp = getenv(env);
    if (tmp) return std::string(tmp);
    else return std::move(def);
}

temporary_postgre_database::temporary_postgre_database() : _user(get_env("MALLARD_USER", "postgres")),
                                                           _password(
                                                                   get_env("MALLARD_PASSWORD", "password")),
                                                           _hostname(get_env("MALLARD_HOSTNAME", "localhost")),
                                                           _port(get_env("MALLARD_PORT", "5432")),
                                                           _name(generate_name()) {
    setup();
}

temporary_postgre_database::temporary_postgre_database(std::string user, std::string password, std::string hostname,
                                                       std::string port)
        : _user(std::move(user)), _password(std::move(password)), _hostname(std::move(hostname)),
          _port(std::move(port)),
          _name(generate_name()) {
    setup();
}

temporary_postgre_database::temporary_postgre_database(std::string user, std::string password, std::string hostname,
                                                       std::string port, std::string name)
        : _user(std::move(user)), _password(std::move(password)), _hostname(std::move(hostname)),
          _port(std::move(port)),
          _name(std::move(name)) {
    setup();
}

temporary_postgre_database::~temporary_postgre_database() {
    _session->disconnect();

    pqxx::connection generic(fmt::format("postgresql://{}:{}@{}:{}", _user, _password, _hostname, _port));
    pqxx::nontransaction tx(generic);
    tx.exec(fmt::format("DROP DATABASE {};", _name));
    tx.commit();
}

void temporary_postgre_database::setup() {
    pqxx::connection generic(fmt::format("postgresql://{}:{}@{}:{}", _user, _password, _hostname, _port));
    pqxx::nontransaction tx(generic);
    tx.exec(fmt::format("CREATE DATABASE {};", _name));

    _session = std::make_unique<pqxx::connection>(
            fmt::format("postgresql://{}:{}@{}:{}/{}", _user, _password, _hostname, _port, _name));
}

const std::string& temporary_postgre_database::user() {
    return _user;
}

const std::string& temporary_postgre_database::password() {
    return _password;
}

const std::string& temporary_postgre_database::hostname() {
    return _hostname;
}

const std::string& temporary_postgre_database::port() {
    return _port;
}

const std::string& temporary_postgre_database::name() {
    return _name;
}

pqxx::connection& temporary_postgre_database::session() {
    return *_session;
}

std::string temporary_postgre_database::generate_name() {
    static size_t name_size = 8;

    std::random_device r;
    std::mt19937 g(r());
    std::uniform_int_distribution dist(97, 122);

    std::string name;
    name.reserve(name_size);
    while (name.size() != name_size) {
        name.push_back(static_cast<char>(dist(g)));
    }
    return name;
}
