-- noinspection SqlNoDataSourceInspectionForFile

-- #!migration
-- name: schema;
CREATE SCHEMA contacts;

-- #!migration
-- name: table/person,
-- requires: [schema];
SET search_path TO contacts;

CREATE TABLE person;

-- #!migration
-- name: table/phone,
-- requires: [table/person];
SET search_path TO contacts;

CREATE TABLE phone;
