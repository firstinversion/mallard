-- noinspection SqlNoDataSourceInspectionForFile

-- #!migration
-- name: table/address,
-- requires: [table/person];
CREATE TABLE address(
  address_id  integer NOT NULL  AUTO_INCREMENT,
  person_id   integer NOT NULL,
  line1       text    NOT NULL,
  line2       text,
  city        text    NOT NULL,
  state       text    NOT NULL,
  zip         text    NOT NULL,

  PRIMARY KEY (address_id),
  FOREIGN KEY (person_id) REFERENCES person(person_id)
);

-- #!migration
-- name: table/email,
-- requires: [table/person];
CREATE TABLE email(
  email_id  integer NOT NULL  AUTO_INCREMENT,
  person_id integer NOT NULL,
  address   text    NOT NULL,

  PRIMARY KEY (email_id),
  FOREIGN KEY (person_id) REFERENCES person(person_id)
);
