-- noinspection SqlNoDataSourceInspectionForFile

-- #!migration
-- name: table/person;
CREATE TABLE person(
  person_id integer PRIMARY KEY,
  name text NOT NULL
);

-- #!migration
-- name: table/phone,
-- requires: [table/person];
CREATE TABLE phone(
  phone_id integer PRIMARY KEY,
  person_id integer NOT NULL,
  number text NOT NULL,

  FOREIGN KEY (person_id) REFERENCES person(person_id)
);
