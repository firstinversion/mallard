-- noinspection SqlNoDataSourceInspectionForFile

-- #!migration
-- name: table/person,
-- requires: [table/phone];
CREATE TABLE person;

-- #!migration
-- name: table/phone,
-- requires: [table/person];
CREATE TABLE phone;
