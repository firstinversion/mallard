-- noinspection SqlNoDataSourceInspectionForFile

-- #!migration
-- name: table/person;
CREATE TABLE person(
  person_id serial    NOT NULL,
  name      text      NOT NULL,

  PRIMARY KEY (person_id)
);

-- #!migration
-- name: table/phone,
-- requires: [table/person];
CREATE TABLE phone(
  phone_id    serial  NOT NULL,
  person_id   integer NOT NULL,
  number      text    NOT NULL,

  PRIMARY KEY (phone_id),
  FOREIGN KEY (person_id) REFERENCES person(person_id)
);
