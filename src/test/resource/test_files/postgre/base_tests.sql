-- #!test
-- name: test1;
SELECT table_name
  FROM information_schema.tables
 WHERE table_schema='public'
   AND table_type='BASE TABLE';

-- #!test
-- name: test2;
SELECT datname FROM pg_database WHERE datistemplate = false;
