#include <mallard/testers/postgre_tester.h>

namespace mallard {

postgre_tester::postgre_tester(pqxx::connection& session) : _session(session) {}

void postgre_tester::apply(const test_plan& plan) {
    for (const auto& t : plan) {
        try {
            auto w = pqxx::work{_session};
            w.exec(t.script_text());
        } catch (const std::exception& e) {
            throw tester_sql_error(t.name(), e);
        }
    }
}

}
