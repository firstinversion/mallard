#include <mallard/test.h>

namespace mallard {

test::test(std::string_view name) : _name(name) {}

test::test(const char* name) : _name(name) {}

const std::string& test::name() const {
    return _name;
}

const std::string& test::description() const {
    return _description;
}

const std::string& test::script_text() const {
    return _script_text;
}

test& test::name(std::string_view name) {
    _name = name;
    return *this;
}

test& test::description(std::string_view description) {
    _description = description;
    return *this;
}

test& test::script_text(std::string_view script_text) {
    _script_text = script_text;
    return *this;
}

test& test::set_name(std::string_view name) {
    _name = name;
    return *this;
}

test& test::set_description(std::string_view description) {
    _description = description;
    return *this;
}

test& test::set_script_text(std::string_view script_text) {
    _script_text = script_text;
    return *this;
}

bool operator==(const test& lhs, const test& rhs) {
    return lhs._name == rhs._name &&
           lhs._description == rhs._description &&
           lhs._script_text == rhs._script_text;
}

bool operator!=(const test& lhs, const test& rhs) {
    return !(rhs == lhs);
}

bool operator<(const test& lhs, const test& rhs) {
    if (lhs._name < rhs._name)
        return true;
    if (rhs._name < lhs._name)
        return false;
    if (lhs._description < rhs._description)
        return true;
    if (rhs._description < lhs._description)
        return false;
    return lhs._script_text < rhs._script_text;
}

bool operator>(const test& lhs, const test& rhs) {
    return rhs < lhs;
}

bool operator<=(const test& lhs, const test& rhs) {
    return !(rhs < lhs);
}

bool operator>=(const test& lhs, const test& rhs) {
    return !(lhs < rhs);
}

}
