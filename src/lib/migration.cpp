#include <mallard/migration.h>

#include <stdexcept>
#include <boost/filesystem.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <fmt/format.h>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>

namespace mallard {

void trim(std::string& str) {
    // trim from start
    str.erase(str.begin(), std::find_if(str.begin(), str.end(), [](int ch) {
        return !(std::isspace(ch) || ch == '\n');
    }));
    // trim from end
    str.erase(std::find_if(str.rbegin(), str.rend(), [](int ch) {
        return !(std::isspace(ch) || ch == '-' || ch == '\n');
    }).base(), str.end());
}

migration::migration(std::string name)
        : _name(std::move(name)) {}

migration::migration(const char* name)
        : _name(name) {}

const std::string& migration::name() const {
    return _name;
}

const std::string& migration::description() const {
    return _description;
}

const std::vector<std::string>& migration::requires_() const {
    return _requires;
}

const std::string& migration::script_text() const {
    return _script_text;
}

const std::string& migration::checksum() const {
    return _checksum;
}

migration& migration::name(std::string name) {
    _name = std::move(name);
    return *this;
}

migration& migration::description(std::string description) {
    _description = std::move(description);
    return *this;
}

migration& migration::requires_(std::vector<std::string> requires_) {
    _requires = std::move(requires_);
    return *this;
}

migration& migration::set_name(std::string name) {
    return migration::name(std::move(name));
}

migration& migration::set_description(std::string description) {
    return migration::description(std::move(description));
}

migration& migration::set_requires_(std::vector<std::string> requires_) {
    return migration::requires_(std::move(requires_));
}

migration& migration::set_script_text(std::string script_text) {
    return migration::script_text(std::move(script_text));
}

migration& migration::script_text(std::string scriptText) {
    _script_text = std::move(scriptText);
    trim(_script_text);

    CryptoPP::byte digest[CryptoPP::SHA256::DIGESTSIZE];
    CryptoPP::SHA256 hash;
    hash.CalculateDigest(digest, (CryptoPP::byte*) _script_text.data(), _script_text.size());

    CryptoPP::HexEncoder encoder;
    encoder.Attach(new CryptoPP::StringSink(_checksum));
    encoder.Put(digest, sizeof(digest));
    encoder.MessageEnd();

    return *this;
}

bool operator==(const migration& lhs, const migration& rhs) {
    return lhs._name == rhs._name &&
           lhs._description == rhs._description &&
           lhs._requires == rhs._requires &&
           lhs._script_text == rhs._script_text;
}

bool operator!=(const migration& lhs, const migration& rhs) {
    return !(rhs == lhs);
}

bool operator<(const migration& lhs, const migration& rhs) {
    if (lhs._name < rhs._name)
        return true;
    if (rhs._name < lhs._name)
        return false;
    if (lhs._description < rhs._description)
        return true;
    if (rhs._description < lhs._description)
        return false;
    if (lhs._requires < rhs._requires)
        return true;
    if (rhs._requires < lhs._requires)
        return false;
    return lhs._script_text < rhs._script_text;
}

bool operator>(const migration& lhs, const migration& rhs) {
    return rhs < lhs;
}

bool operator<=(const migration& lhs, const migration& rhs) {
    return !(rhs < lhs);
}

bool operator>=(const migration& lhs, const migration& rhs) {
    return !(lhs < rhs);
}

}
