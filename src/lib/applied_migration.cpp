#include <mallard/applied_migration.h>

namespace mallard {

applied_migration::applied_migration()
        : _database_id(-1) {}

int64_t applied_migration::database_id() const {
    return _database_id;
}

const std::string& applied_migration::migration_name() const {
    return _migration_name;
}

const std::string& applied_migration::checksum() const {
    return _checksum;
}

std::chrono::time_point<std::chrono::system_clock> applied_migration::applied_on() const {
    return _applied_on;
}

applied_migration& applied_migration::database_id(int64_t id) {
    _database_id = id;
    return *this;
}

applied_migration& applied_migration::migration_name(std::string name) {
    _migration_name = std::move(name);
    return *this;
}

applied_migration& applied_migration::checksum(std::string checksum) {
    _checksum = std::move(checksum);
    return *this;
}

applied_migration& applied_migration::applied_on(std::chrono::time_point<std::chrono::system_clock> applied_on) {
    _applied_on = applied_on;
    return *this;
}

}
