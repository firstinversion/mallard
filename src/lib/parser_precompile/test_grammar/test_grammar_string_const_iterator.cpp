#ifdef MALLARD_PRECOMPILE_GRAMMAR

#include <mallard/test_parser.h>

template
class mallard::test_grammar<std::string::const_iterator, mallard::mallard_temp_test_skipper<std::string::const_iterator>>;

#endif
