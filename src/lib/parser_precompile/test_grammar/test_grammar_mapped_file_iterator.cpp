#ifdef MALLARD_PRECOMPILE_GRAMMAR

#include <mallard/test_parser.h>

template
class mallard::test_grammar<boost::iostreams::mapped_file::iterator,
        mallard::mallard_temp_test_skipper<boost::iostreams::mapped_file::iterator>>;

#endif
