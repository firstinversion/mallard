#ifdef MALLARD_PRECOMPILE_GRAMMAR

#include <mallard/migration_parser.h>

template
class mallard::migration_grammar<std::string::iterator, mallard::mallard_default_skipper<std::string::iterator>>;

#endif
