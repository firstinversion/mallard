#ifdef MALLARD_PRECOMPILE_GRAMMAR

#include <mallard/migration_parser.h>

template
class mallard::migration_grammar<std::string::const_iterator, mallard::mallard_default_skipper<std::string::const_iterator>>;

#endif
