#ifdef MALLARD_PRECOMPILE_GRAMMAR

#include <mallard/migration_parser.h>

template
class mallard::migration_grammar<boost::iostreams::mapped_file::iterator,
        mallard::mallard_default_skipper<boost::iostreams::mapped_file::iterator>>;

#endif
