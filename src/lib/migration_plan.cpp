#include <mallard/migration_plan.h>

#include <boost/graph/strong_components.hpp>
#include <boost/graph/topological_sort.hpp>

namespace mallard {

migration_plan::migration_plan()
        : _is_valid(false) {}

migration_plan::migration_plan(std::vector<migration> migrations)
        : _migrations(std::move(migrations)), _is_valid(false) {}

migration_plan::migration_plan(std::initializer_list<migration> migrations)
        : _migrations(migrations), _is_valid(false) {}

migration& migration_plan::at(size_t idx) {
    _is_valid = false;
    return _migrations.at(idx);
}

const migration& migration_plan::at(size_t idx) const {
    return _migrations.at(idx);
}

migration& migration_plan::operator[](size_t idx) {
    _is_valid = false;
    return _migrations[idx];
}

const migration& migration_plan::operator[](size_t idx) const {
    return _migrations[idx];
}

migration& migration_plan::front() {
    _is_valid = false;
    return _migrations.front();
}

const migration& migration_plan::front() const {
    return _migrations.front();
}

migration& migration_plan::back() {
    _is_valid = false;
    return _migrations.back();
}

const migration& migration_plan::back() const {
    return _migrations.back();
}

migration* migration_plan::data() noexcept {
    _is_valid = false;
    return _migrations.data();
}

const migration* migration_plan::data() const noexcept {
    return _migrations.data();
}

migration_plan::iterator migration_plan::begin() noexcept {
    _is_valid = false;
    return _migrations.begin();
}

migration_plan::const_iterator migration_plan::begin() const noexcept {
    return _migrations.begin();
}

migration_plan::const_iterator migration_plan::cbegin() const noexcept {
    return _migrations.cbegin();
}

migration_plan::iterator migration_plan::end() noexcept {
    _is_valid = false;
    return _migrations.end();
}

migration_plan::const_iterator migration_plan::end() const noexcept {
    return _migrations.end();
}

migration_plan::const_iterator migration_plan::cend() const noexcept {
    return _migrations.cend();
}

migration_plan::reverse_iterator migration_plan::rbegin() noexcept {
    _is_valid = false;
    return _migrations.rbegin();
}

migration_plan::const_reverse_iterator migration_plan::rbegin() const noexcept {
    return _migrations.rbegin();
}

migration_plan::const_reverse_iterator migration_plan::crbegin() const noexcept {
    return _migrations.crbegin();
}

migration_plan::reverse_iterator migration_plan::rend() noexcept {
    _is_valid = false;
    return _migrations.rend();
}

migration_plan::const_reverse_iterator migration_plan::rend() const noexcept {
    return _migrations.rend();
}

migration_plan::const_reverse_iterator migration_plan::crend() const noexcept {
    return _migrations.crend();
}

bool migration_plan::empty() const noexcept {
    return _migrations.empty();
}

migration_plan::size_type migration_plan::size() const noexcept {
    return _migrations.size();
}

migration_plan::size_type migration_plan::max_size() const noexcept {
    return _migrations.max_size();
}

void migration_plan::reserve(migration_plan::size_type new_cap) {
    _migrations.reserve(new_cap);
}

migration_plan::size_type migration_plan::capacity() const noexcept {
    return _migrations.capacity();
}

void migration_plan::shrink_to_fit() {
    _migrations.shrink_to_fit();
}

void migration_plan::clear() noexcept {
    _is_valid = false;

    _migrations.clear();
    _names.clear();
    _graph.clear();
}

migration_plan::iterator migration_plan::insert(migration_plan::const_iterator pos, migration value) {
    _is_valid = false;
    return _migrations.insert(pos, std::move(value));
}

migration_plan::iterator
migration_plan::insert(migration_plan::const_iterator pos, migration_plan::size_type count, const migration& value) {
    _is_valid = false;
    return _migrations.insert(pos, count, value);
}

migration_plan::iterator
migration_plan::insert(migration_plan::const_iterator pos, std::initializer_list<migration> ilist) {
    _is_valid = false;
    return _migrations.insert(pos, ilist);
}

migration_plan::iterator migration_plan::erase(migration_plan::const_iterator pos) {
    _is_valid = false;
    return _migrations.erase(pos);
}

migration_plan::iterator
migration_plan::erase(migration_plan::const_iterator first, migration_plan::const_iterator last) {
    _is_valid = false;
    return _migrations.erase(first, last);
}

void migration_plan::push_back(migration value) {
    _is_valid = false;
    _migrations.push_back(std::move(value));
}

void migration_plan::pop_back() {
    _is_valid = false;
    _migrations.pop_back();
}

void migration_plan::resize(migration_plan::size_type count) {
    _is_valid = false;
    _migrations.resize(count);
}

void migration_plan::resize(migration_plan::size_type count, const migration& value) {
    _is_valid = false;
    _migrations.resize(count, value);
}

void migration_plan::swap(migration_plan& other) noexcept {
    _is_valid = false;
    other._is_valid = false;

    _migrations.swap(other._migrations);
}

bool migration_plan::valid() const {
    return _is_valid;
}

errors migration_plan::validate() {
    errors report;
    bool can_continue = true;

    // Clear calculated data
    _names.clear();
    _graph.clear();
    _topological_ordering.clear();

    // Perform validations
    can_continue = validate_no_name_overlap(report);
    if (can_continue) can_continue = validate_no_missing_dependency(report);
    if (can_continue) validate_no_dependency_cycle(report);

    _is_valid = report.empty();
    if (_is_valid) {
        _topological_ordering.reserve(_migrations.size());
        boost::topological_sort(_graph, std::back_inserter(_topological_ordering));
    }
    return report;
}

bool migration_plan::validate_no_name_overlap(errors& report) {
    for (size_t i = 0; i < _migrations.size(); i++) {
        const auto& n = _migrations[i].name();
        if (_names.count(n)) report.push_back(std::make_unique<name_overlap_error>(n));
        _names[n] = i;
    }
    return true;
}

bool migration_plan::validate_no_missing_dependency(errors& report) {
    for (size_t i = 0; i < _migrations.size(); i++) {
        const auto& m = _migrations[i];

        for (const auto& r : m.requires_()) {
            auto itr = _names.find(r);
            if (itr == _names.end()) {
                report.push_back(std::make_unique<missing_dependency_error>(m.name(), r));
            } else {
                boost::add_edge(i, itr->second, _graph);
            }
        }
    }

    return true;
}

bool migration_plan::validate_no_dependency_cycle(errors& report) {
    std::vector<size_t> component_map(boost::num_vertices(_graph));
    size_t component_count = boost::strong_components(_graph, boost::make_iterator_property_map(component_map.begin(),
                                                                                                boost::get(
                                                                                                        boost::vertex_index,
                                                                                                        _graph)));
    if (component_count == component_map.size()) return true;

    std::vector<size_t> components(component_count);
    for (auto count : component_map) components[count]++;

    for (size_t comp_i = 0; comp_i < components.size(); comp_i++) {
        if (components[comp_i] > 1) {
            std::vector<std::string> names;
            for (size_t vert_i = 0; vert_i < component_map.size(); vert_i++) {
                if (component_map[vert_i] == comp_i) {
                    names.push_back(_migrations[vert_i].name());
                }
            }

            report.push_back(std::make_unique<dependency_cycle_error>(std::move(names)));
        }
    }

    return false;
}

bool operator==(const migration_plan& lhs, const migration_plan& rhs) {
    return lhs._migrations == rhs._migrations;
}

bool operator!=(const migration_plan& lhs, const migration_plan& rhs) {
    return !(rhs == lhs);
}

bool operator<(const migration_plan& lhs, const migration_plan& rhs) {
    return lhs._migrations < rhs._migrations;
}

bool operator>(const migration_plan& lhs, const migration_plan& rhs) {
    return rhs < lhs;
}

bool operator<=(const migration_plan& lhs, const migration_plan& rhs) {
    return !(rhs < lhs);
}

bool operator>=(const migration_plan& lhs, const migration_plan& rhs) {
    return !(lhs < rhs);
}

migration_plan::topological_iterator migration_plan::tbegin() const {
    return topological_iterator(_migrations, _topological_ordering.cbegin());
}

migration_plan::topological_iterator migration_plan::tend() const {
    return topological_iterator(_migrations, _topological_ordering.cend());
}

migration_plan::topological_iterator::topological_iterator(const std::vector<migration>& migrations,
                                                           std::vector<size_t>::const_iterator base_iterator)
        : _migrations(migrations), _base_iterator(base_iterator) {}

const migration& migration_plan::topological_iterator::operator*() const {
    auto idx = *_base_iterator;
    return _migrations[idx];
}

const migration* migration_plan::topological_iterator::operator->() const {
    auto idx = *_base_iterator;
    return &_migrations[idx];
}

migration_plan::topological_iterator& migration_plan::topological_iterator::operator++() {
    _base_iterator++;
    return *this;
}

const migration_plan::topological_iterator migration_plan::topological_iterator::operator++(int) {
    const auto tmp(*this);
    _base_iterator++;
    return tmp;
}

bool operator==(const migration_plan::topological_iterator& lhs, const migration_plan::topological_iterator& rhs) {
    return lhs._base_iterator == rhs._base_iterator;
}

bool operator!=(const migration_plan::topological_iterator& lhs, const migration_plan::topological_iterator& rhs) {
    return !(rhs == lhs);
}

}
