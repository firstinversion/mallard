#include <mallard/applicators/postgre_applicator.h>

namespace mallard {

namespace postgre {

const char* script_version_1 =
        "CREATE SCHEMA mallard;"
        "CREATE TABLE mallard.version("
        "   version     serial      NOT NULL,"
        "   applied_on  timestamptz NOT NULL DEFAULT now()"
        ");"
        "CREATE TABLE mallard.applied_migration("
        "   id          serial      NOT NULL,"
        "   name        text        NOT NULL,"
        "   description text,"
        "   requires    text[],"
        "   checksum    text        NOT NULL,"
        "   script_text text        NOT NULL,"
        "   applied_on  timestamptz NOT NULL DEFAULT now(),"
        "   PRIMARY KEY (id)"
        ");"
        "INSERT INTO mallard.version (version) VALUES (1);";
}

postgre_applicator::postgre_applicator(pqxx::connection& session, bool auto_initialize)
        : _session(session) {
    if (auto_initialize) initialize();
}

void postgre_applicator::initialize() {
    int32_t version = find_version();
    switch (version) {
        case 0:
            setup_version_1();
        default:
            break;
    }
}

int32_t postgre_applicator::find_version() {
    pqxx::work transaction(_session);
    pqxx::result row_names = transaction.exec(
            "SELECT table_name\n"
            "FROM information_schema.tables\n"
            "WHERE table_schema = 'mallard' AND table_name = 'version';");

    if (row_names.begin() == row_names.end()) {
        transaction.commit();
        return 0;
    } else {
        pqxx::result version = transaction.exec("SELECT MAX(version) FROM mallard.version;");
        transaction.commit();
        return (*version.begin())[0].as<int32_t>();
    }
}

void postgre_applicator::setup_version_1() {
    pqxx::work transaction(_session);
    transaction.exec(postgre::script_version_1);
    transaction.commit();
}

std::string postgre_applicator::stringify_requires(const std::vector<std::string>& req) {
    std::stringstream ss;
    ss << '{';
    for (size_t i = 0; i < req.size(); ++i) {
        ss << req[i];
        if (i < req.size() - 1) ss << ',';
    }
    ss << '}';
    return ss.str();
}

void postgre_applicator::apply(const migration_plan& plan, std::function<void(const migration&)> on_apply) {
    std::vector<applied_migration> applied;
    find_applied_migrations(std::back_inserter(applied));

    std::unordered_set<std::string> names;
    std::transform(applied.begin(), applied.end(), std::inserter(names, names.end()),
                   [](auto& a) { return a.migration_name(); });

    for (auto itr = plan.tbegin(); itr != plan.tend(); ++itr) {
        if (names.count(itr->name()) == 0) {
            try {
                pqxx::work transaction(_session);
                transaction.exec(itr->script_text());
                record_migration(transaction, *itr);
                transaction.commit();
                on_apply(*itr);
            } catch (const std::exception& e) {
                throw applicator_sql_error(itr->name(), e);
            }
        }
    }
}

void postgre_applicator::record_migration(pqxx::work& tx, const migration& migration) {
    std::string string_requires = stringify_requires(migration.requires_());
    tx.exec_params(
            "INSERT INTO mallard.applied_migration(name, description, requires, checksum, script_text)"
            "VALUES ($1, $2, $3, $4, $5);",
            migration.name(),
            migration.description(),
            string_requires,
            migration.checksum(),
            migration.script_text());
}

}
