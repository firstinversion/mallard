#include <mallard/applicators/sqlite_applicator.h>

#include <sstream>
#include <unordered_set>

namespace mallard {

const char* script_version =
        "CREATE TABLE mallard_version (\n"
        "  version     integer   PRIMARY KEY,\n"
        "  applied_on  integer   NOT NULL  DEFAULT (DATETIME('now'))\n"
        ");";
const char* script_applied_migrations =
        "CREATE TABLE mallard_applied_migration (\n"
        "  id            integer   PRIMARY KEY,\n"
        "  name          text      NOT NULL,\n"
        "  description   text,\n"
        "  requires      text,\n"
        "  checksum      text      NOT NULL,\n"
        "  script_text   text      NOT NULL,\n"
        "  applied_on    integer   NOT NULL  DEFAULT (DATETIME('now'))\n"
        ");";
const char* script_set_version_1 =
        "INSERT INTO mallard_version (version) VALUES (1);";

sqlite_applicator::sqlite_applicator(const std::string& connection_string, bool auto_initialize)
        : _session(std::make_unique<SQLite::Database>(connection_string, SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE)) {
    if (auto_initialize) initialize();
}

sqlite_applicator::sqlite_applicator(std::unique_ptr<SQLite::Database> session, bool auto_initialize)
        : _session(std::move(session)) {
    if (auto_initialize) initialize();
}

void sqlite_applicator::initialize() {
    if (!_session) throw applicator_missing_session_error();
    int64_t version = find_version();
    switch (version) {
        case 0:
            setup_version_1();
        default:
            break;
    }
}

int64_t sqlite_applicator::find_version() {
    if (!_session) throw applicator_missing_session_error();
    SQLite::Statement row_names(*_session,
                                "SELECT name FROM sqlite_master WHERE type='table' AND name='mallard_version';");

    if (row_names.executeStep()) {
        SQLite::Statement version(*_session, "SELECT max(version) FROM mallard_version;");
        version.executeStep();
        return version.getColumn(0).getInt64();
    } else {
        return 0;
    }
}

void sqlite_applicator::record_migration(const migration& migration) {
    std::string string_requires = stringify_requires(migration.requires_());
    SQLite::Statement insert(*_session,
            "INSERT INTO mallard_applied_migration (name, description, requires, checksum, script_text)"
            "VALUES (:name, :desc, :req, :checksum, :script);");
    insert.bind(":name", migration.name());
    insert.bind(":desc", migration.description());
    insert.bind(":req", string_requires);
    insert.bind(":checksum", migration.checksum());
    insert.bind(":script", migration.script_text());
    insert.exec();
}

std::string sqlite_applicator::stringify_requires(const std::vector<std::string>& req) {
    std::stringstream ss;
    for (size_t i = 0; i < req.size(); ++i) {
        ss << req[i];
        if (i < req.size() - 1) ss << ',';
    }
    return ss.str();
}

void sqlite_applicator::setup_version_1() {
    SQLite::Statement version(*_session, script_version);
    version.exec();

    SQLite::Statement applied_migrations(*_session, script_applied_migrations);
    applied_migrations.exec();

    SQLite::Statement set_version_1(*_session, script_set_version_1);
    set_version_1.exec();
}

SQLite::Database& sqlite_applicator::session() {
    if (!_session) throw applicator_missing_session_error();
    return *_session;
}

void sqlite_applicator::apply(const migration_plan& plan) {
    if (!_session) throw applicator_missing_session_error();

    std::vector<applied_migration> applied;
    find_applied_migrations(std::back_inserter(applied));

    std::unordered_set<std::string> names;
    std::transform(applied.begin(), applied.end(), std::inserter(names, names.end()),
                   [](auto& a) { return a.migration_name(); });

    for (auto itr = plan.tbegin(); itr != plan.tend(); ++itr) {
        if (names.count(itr->name()) == 0) {
            SQLite::Statement script(*_session, itr->script_text());
            script.exec();
            record_migration(*itr);
        }
    }
}

std::unique_ptr<SQLite::Database> sqlite_applicator::extract_session() {
    if (!_session) throw applicator_missing_session_error();
    return std::move(_session);
}

}
