#include <mallard/error.h>

#include <sstream>
#include <fmt/format.h>

namespace mallard {

name_overlap_error::name_overlap_error(std::string name)
        : _name(std::move(name)) {
    _what = fmt::format("The name {} is claimed by multiple migration scripts.", _name);
}

const char* name_overlap_error::what() const noexcept {
    return _what.data();
}

const std::string& name_overlap_error::name() {
    return _name;
}

missing_dependency_error::missing_dependency_error(std::string migration_name, std::string dependency_name)
        : _migration_name(std::move(migration_name)), _dependency_name(std::move(dependency_name)) {
    _what = fmt::format("{} depends on {}, but it could not be found.", _migration_name, _dependency_name);
}

const char* missing_dependency_error::what() const noexcept {
    return _what.data();
}

const std::string& missing_dependency_error::migration_name() {
    return _migration_name;
}

const std::string& missing_dependency_error::dependency_name() {
    return _dependency_name;
}

dependency_cycle_error::dependency_cycle_error(std::vector<std::string> names)
        : _names(std::move(names)) {
    std::stringstream ss;
    ss << "Dependency Cycle:\n";
    for (const auto& n : _names) {
        ss << "\t" << n << "\n";
    }
    _what = ss.str();
}

const char* dependency_cycle_error::what() const noexcept {
    return _what.data();
}

const std::vector<std::string>& dependency_cycle_error::getNames() const {
    return _names;
}

parsing_error::parsing_error(std::string what)
        : _what(std::move(what)) {}

parsing_error::parsing_error(const char* what)
        : _what(what) {}

const char* parsing_error::what() const noexcept {
    return _what.data();
}

const char* applicator_missing_session_error::what() const noexcept {
    static const char* msg = "applicator was called after session was extracted.";
    return msg;
}

applicator_sql_error::applicator_sql_error(std::string migration_name, const std::exception& sqlerror)
        : _migration_name(std::move(migration_name)),
          _what(fmt::format("SQL Exception occurred when applying {}:\n\t{}\n", _migration_name, sqlerror.what())) {}

const char* applicator_sql_error::what() const noexcept {
    return _what.data();
}

tester_sql_error::tester_sql_error(std::string test_name, const std::exception& sqlerror)
        : _test_name(std::move(test_name)),
          _what(fmt::format("SQL Exception occurred when testing {}:\n\t{}\n", _test_name, sqlerror.what())) {}

const char* tester_sql_error::what() const noexcept {
    return _what.data();
}

}
