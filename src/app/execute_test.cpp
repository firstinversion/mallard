#include "execute_test.h"

#include <fmt/format.h>
#include <boost/filesystem/path.hpp>
#include "files.h"
#include <mallard/test_plan.h>
#include <mallard/test_parser.h>
#include <mallard/testers/postgre_tester.h>

void execute_test(const std::string& root, const std::string& backend, const std::string& connstr) {
    boost::filesystem::path root_path = ensure_root(root);

    std::vector<boost::filesystem::path> source_files;
    discover_sources(source_files, root_path);

    mallard::test_plan plan;
    std::for_each(source_files.begin(), source_files.end(), [&](const boost::filesystem::path& p) {
        mallard::parse_test_from_file(p.string(), std::back_inserter(plan));
    });

    if (backend == "postgresql") {
        pqxx::connection session(connstr);
        mallard::postgre_tester tester(session);
        tester.apply(plan);
    } else if (backend == "sqlite") {
        throw std::runtime_error("SQLite not yet supported for testing.");
    } else {
        fmt::print("Backend {} not supported.", backend);
    }
}
