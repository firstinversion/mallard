#include <iostream>
#include <fmt/format.h>
#include <iostream>
#include <boost/program_options.hpp>
#include "execute_migrate.h"
#include "execute_test.h"

namespace po = boost::program_options;

po::options_description buildGlobalOptionsDescription() {
    po::options_description desc("Global Options");
    desc.add_options()
            ("command", po::value<std::string>(), "Command to execute.")
            ("subargs", po::value<std::vector<std::string>>(), "Arguments for command.");

    return desc;
}

po::positional_options_description buildGlobalPositionDescription() {
    po::positional_options_description pos;
    pos.add("command", 1).add("subargs", -1);

    return pos;
}

po::options_description buildMigrateOptionsDescription() {
    po::options_description desc("Migrate Options");
    desc.add_options()
            ("root", po::value<std::string>(), "Root migration Script directory.")
            ("backend", po::value<std::string>(), "Database backend engine.")
            ("connstr", po::value<std::string>(), "Database connection string.");

    return desc;
}

po::options_description buildTestOptionsDescription() {
    po::options_description desc("Test Options");
    desc.add_options()
            ("root", po::value<std::string>(), "Root test Script directory.")
            ("backend", po::value<std::string>(), "Database backend engine.")
            ("connstr", po::value<std::string>(), "Database connection string.");
    return desc;
}

void executeMigrate(const po::parsed_options& globalParsed) {
    po::options_description desc = buildMigrateOptionsDescription();
    std::vector<std::string> remaining = po::collect_unrecognized(globalParsed.options, po::include_positional);

    po::variables_map vm;
    po::parsed_options parsed = po::command_line_parser(remaining).options(desc).run();
    po::store(parsed, vm);

    if (!vm.count("root")) throw std::invalid_argument("Root Script directory not provided.");
    if (!vm.count("backend")) throw std::invalid_argument("Database backend not provided.");
    if (!vm.count("connstr")) throw std::invalid_argument("Database connection string not provided.");

    execute_migrate(vm["root"].as<std::string>(), vm["backend"].as<std::string>(), vm["connstr"].as<std::string>());
}

void executeTest(const po::parsed_options& globalParsed) {
    po::options_description desc = buildTestOptionsDescription();
    std::vector<std::string> remaining = po::collect_unrecognized(globalParsed.options, po::include_positional);

    po::variables_map vm;
    po::parsed_options parsed = po::command_line_parser(remaining).options(desc).run();
    po::store(parsed, vm);

    if (!vm.count("root")) throw std::invalid_argument("Root test Script directory not provided.");
    if (!vm.count("backend")) throw std::invalid_argument("Database backend not provided.");
    if (!vm.count("connstr")) throw std::invalid_argument("Database connection string not provided.");

    execute_test(vm["root"].as<std::string>(), vm["backend"].as<std::string>(), vm["connstr"].as<std::string>());
}

int main(int argc, char** argv) {
    po::options_description globalOpts = buildGlobalOptionsDescription();
    po::positional_options_description globalPos = buildGlobalPositionDescription();
    po::variables_map vm;
    po::parsed_options parsed = po::command_line_parser(argc, argv)
            .options(globalOpts)
            .positional(globalPos)
            .allow_unregistered()
            .run();

    po::store(parsed, vm);

    try {
        if (!vm.count("command")) throw std::invalid_argument("Command was not given.");

        std::string cmd = vm["command"].as<std::string>();

        if (cmd == "migrate") {
            executeMigrate(parsed);
        } else if (cmd == "test") {
            executeTest(parsed);
        } else if (cmd == "help") {
            std::cout << globalOpts << '\n';
            return 1;
//            boost::process::child c("man mallard");
//            c.wait();
            // Boost process does not work under OSX for no reason that is reasonable.
        } else {
            throw std::invalid_argument(
                    fmt::format("{} is not a known command. Try 'mallard help' for the manual.", cmd));
        }

        return EXIT_SUCCESS;
    }
    catch (const std::invalid_argument& e) {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }
    catch (const std::exception& e) {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }
}

