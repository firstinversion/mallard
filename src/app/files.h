#pragma once

#include <boost/filesystem/path.hpp>
#include <vector>
#include <string>

boost::filesystem::path ensure_root(std::string root);

void discover_sources(std::vector<boost::filesystem::path>& sources, const boost::filesystem::path& root);
