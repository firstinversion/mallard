#include "execute_migrate.h"

#include <algorithm>
#include <boost/filesystem.hpp>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <mallard/migration_plan.h>
#include <mallard/migration_parser.h>
#include <mallard/applicators/sqlite_applicator.h>
#include <mallard/applicators/postgre_applicator.h>
#include "files.h"

void execute_migrate(const std::string& root, const std::string& backend, const std::string& connstr) {
    boost::filesystem::path root_path = ensure_root(root);

    std::vector<boost::filesystem::path> source_files;
    discover_sources(source_files, root_path);

    mallard::migration_plan plan;
    std::for_each(source_files.begin(), source_files.end(), [&](const boost::filesystem::path& p) {
        mallard::parse_migration_from_file(p.string(), std::back_inserter(plan));
    });

    if (backend == "postgresql") {
        pqxx::connection session(connstr);
        mallard::postgre_applicator applicator(session);
        auto errors = plan.validate();
        if (!errors.empty()) {
            for (auto& e : errors) fmt::print("    {}\n", e->what());
            throw std::runtime_error("Bad migration plan.");
        }
        applicator.apply(plan, [](const auto& migration) {
            fmt::print("+ {}\n", migration.name());
        });
    } else if (backend == "sqlite") {
        mallard::sqlite_applicator applicator(connstr);
        auto errors = plan.validate();
        if (!errors.empty()) {
            for (auto& e : errors) fmt::print("    {}\n", e->what());
            throw std::runtime_error("Bad migration plan.");
        }
        applicator.apply(plan);
    } else {
        fmt::print("Backend {} not supported.", backend);
    }
}
