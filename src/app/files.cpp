#include "files.h"

#include <boost/filesystem.hpp>
#include <fmt/format.h>
#include <fmt/ostream.h>

boost::filesystem::path ensure_root(std::string root) {
    boost::filesystem::path root_path(root);
    if (!boost::filesystem::exists(root_path))
        throw std::invalid_argument(fmt::format("Root path given did not exist: {}", root_path));
    if (!boost::filesystem::is_directory(root_path))
        throw std::invalid_argument(fmt::format("Root path given was not a directory: {}", root_path));
    return root_path;
}

void discover_sources(std::vector<boost::filesystem::path>& sources, const boost::filesystem::path& root) {
    for (const boost::filesystem::directory_entry& e : boost::filesystem::directory_iterator(root)) {
        if (boost::filesystem::is_regular_file(e.path())) {
            sources.push_back(e.path());
        } else if (boost::filesystem::is_directory(e.path())) {
            discover_sources(sources, e.path());
        } else {
            fmt::print("Entry was not a directory or file: {}", e.path());
        }
    }
}
