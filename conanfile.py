from conans import ConanFile, CMake, tools


class Mallard(ConanFile):
    name = "mallard"
    version = "0.2.3"
    license = "MIT"
    author = "Andrew Rademacher <andrewrademacher@icloud.com>"
    url = "https://bitbucket.org/firstinversion/mallard"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources = "*"
    requires = ("boost/1.71.0@conan/stable",
                "spdlog/1.4.2@bincrafters/stable",
                "cryptopp/8.2.0@bincrafters/stable",
                "sqlitecpp/2.4.0@bincrafters/stable",
                "libpqxx/6.4.5@bincrafters/stable")
    build_requires = "Catch2/2.10.2@catchorg/stable"

    # def configure(self):
    #     self.options["libpq"].with_openssl = True

    def build(self):
        cmake = CMake(self)
        cmake.configure(defs={'USE_CONAN': True, 'BUILD_STATIC': not self.options.shared})
        cmake.build()
        if tools.get_env("CONAN_RUN_TEST", False):
            cmake.test(output_on_failure=True)

    def package(self):
        cmake = CMake(self)
        cmake.install()

        # self.copy("*.h", dst="include", src="include")
        # self.copy("*.hpp", dst="include", src="include")
        # self.copy("*.lib", dst="lib", keep_path=False)
        # self.copy("*.dll", dst="bin", keep_path=False)
        # self.copy("*.dylib*", dst="lib", keep_path=False)
        # self.copy("*.so", dst="lib", keep_path=False)
        # self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["mallard"]
